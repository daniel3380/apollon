'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.addColumn(
        'user',
        'testColumn',
        {
          type: Sequelize.TEXT,
          allowNull: true,
        },
        { transaction },
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },

  down: async (queryInterface) => {
    await queryInterface.removeColumn('user', 'testColumn');
  },
};

// run this command at service root directory: npx sequelize-cli db:migrate --env development
// run this command at service root directory: npx sequelize-cli db:seed:all --env development
