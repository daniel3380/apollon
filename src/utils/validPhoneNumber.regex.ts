export function validNumberWithRegex(phone: string) {
  const regex = new RegExp(
    /^(098|0098|98|\+98|0)?9(0[0-5]|[1 3]\d|2[0-3]|9[0-9]|41)\d{7}$/g,
  );
  return regex.test(phone);
}
