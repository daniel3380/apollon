import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ThrottlerModule } from '@nestjs/throttler';
import { AdminModule } from './application/admin/admin.module';
import { CommonModule } from './application/common/common.module';
import { UsersModule } from './application/user/user.module';
import { Jwt } from './common/helpers/jwt.helper';
import { UsernameMiddleware } from './common/middlewares/username.middleware';
import { SmsRqDataAccess } from './dataAccess/smsRq.dataAccess';
import { UserDataAccess } from './dataAccess/user.dataAccess';
import sequelizeObj from './database/sequelize.obj';

@Module({
  imports: [
    // ServeStaticModule.forRoot({
    //   rootPat h: join(__dirname, '..', 'public'),
    // }),
    // ConfigModule.forRoot({
    //   isGlobal: true,
    // }),
    sequelizeObj,
    ThrottlerModule.forRoot({
      ttl: 60,
      limit: 10,
    }),
    AdminModule,
    CommonModule,
    UsersModule,
  ],
  controllers: [],
  providers: [UsernameMiddleware, SmsRqDataAccess, UserDataAccess, Jwt],
})
export class AppModule {}
