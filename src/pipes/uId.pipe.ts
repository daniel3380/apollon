import {
  HttpException,
  HttpStatus,
  Injectable,
  PipeTransform,
} from '@nestjs/common';

@Injectable()
export class UIdPipe implements PipeTransform {
  transform(value: any) {
    let { id } = value;
    if (!id) {
      return value;
    }
    if (`${id}`.length !== 36) {
      throw new HttpException(
        {
          status: HttpStatus.FAILED_DEPENDENCY,
          error: '  به درسی وارد نشده است UId ',
        },
        HttpStatus.FAILED_DEPENDENCY,
      );
    }
    return value;
  }
}
