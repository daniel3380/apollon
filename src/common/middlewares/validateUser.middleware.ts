import {
  HttpException,
  HttpStatus,
  Injectable,
  NestMiddleware,
} from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import { SmsRqDataAccess } from '../../dataAccess/smsRq.dataAccess';
import { UserDataAccess } from '../../dataAccess/user.dataAccess';
import { Jwt } from '../helpers/jwt.helper';

@Injectable()
export class ValidUserMiddleware implements NestMiddleware {
  constructor(
    private readonly jwt: Jwt,
    private readonly usersDataAccess: UserDataAccess,
  ) {}
  async use(req: Request, res: Response, next: NextFunction) {
    // if (req.session.user && req.session.user.isOwner) {
    //   res.locals.user = req.session.user;
    //   next();
    // } else
    if (req.header('jToken') && req.header('jToken').length > 0) {
      const token = req.header('jToken');
      const tokenValues = this.jwt.verifier(token);

      if (!tokenValues) {
        next(
          new HttpException(
            {
              status: HttpStatus.UNAUTHORIZED,
              error: ' توکن نامعتبر ',
            },
            HttpStatus.UNAUTHORIZED,
          ),
        );
        return;
      }

      if (typeof tokenValues === 'object') {
        if (Date.now() >= tokenValues.exp * 1000) {
          next(
            new HttpException(
              {
                status: HttpStatus.GATEWAY_TIMEOUT,
                error: ' تایم توکن به اتمام رسیده',
              },
              HttpStatus.GATEWAY_TIMEOUT,
            ),
          );
          return;
        }
      }

      const user = await this.usersDataAccess.findUserToken(token);
      if (!user) {
        next(
          new HttpException(
            {
              status: HttpStatus.UNAUTHORIZED,
              error: ' توکن نامعتبر ',
            },
            HttpStatus.UNAUTHORIZED,
          ),
        );
        return;
      }
      res.locals.user = user;
      next();
      return;
    } else {
      next(
        new HttpException(
          {
            status: HttpStatus.UNAUTHORIZED,
            error: ' برای دسترسی باید وارد شوید ',
          },
          HttpStatus.UNAUTHORIZED,
        ),
      );
      return;
    }
  }
}
@Injectable()
export class CheckUserMiddleware implements NestMiddleware {
  constructor(
    private readonly smsRqDataAccess: SmsRqDataAccess,

    private readonly usersDataAccess: UserDataAccess,
    private readonly jwt: Jwt,
  ) {}
  async use(req: Request, res: Response, next: NextFunction) {
    // query

    this.smsRqDataAccess.deleteSmsRqs();

    // if (req.session.user) {
    //   res.locals.user = req.session.user;
    //   next();
    // } else

    if (req.header('jToken') && req.header('jToken').length > 0) {
      const token = req.header('jToken');
      const tokenValues = this.jwt.verifier(token);
      if (!tokenValues) {
        next();
        return;
      }
      const user = await this.usersDataAccess.findUserToken(token);
      if (!user) {
        next();
        return;
      } else {
        // const user = userMidObj(user, token);
        // req.session.user = user;
        res.locals.user = user;
        next();
        return;
      }
    } else {
      next();
      return;
    }
  }
}
