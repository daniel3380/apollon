const commentStatuses = {
  published: {
    code: 1,
    text: 'published',
  },
  deActived: {
    code: 2,
    text: 'deActived',
  },
  archived: {
    code: 3,
    text: 'archived',
  },
};
export default commentStatuses;
