const blogStatuses = {
  newRequest: {
    code: 0,
    text: 'pending',
  },
  active: {
    code: 1,
    text: 'active',
  },
  inactive: {
    code: 2,
    text: 'inactive',
  },
  archived: {
    code: 3,
    text: 'archived',
  },
};
export default blogStatuses;
