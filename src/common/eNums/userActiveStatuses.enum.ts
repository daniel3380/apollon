const userActiveStatuses = {
  active: 'active',
  deActive: 'deActive',
  suspended: 'suspended',
};
export default userActiveStatuses;
export enum UserActiveStatuses {
  'active' = 'active',
  'deActive' = 'deActive',
  'suspended' = 'suspended',
}
