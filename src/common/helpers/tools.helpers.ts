import smsRequest = require('request');
// var Kavenegar = require('kavenegar');
const env = process.env.NODE_ENV;

export class Tools {
  convertToSlug(Text) {
    return Text.toLowerCase()
      .replace(/ /g, '-')
      .replace(/-{6}/g, '-')
      .replace(/-{5}/g, '-')
      .replace(/-{4}/g, '-')
      .replace(/-{3}/g, '-')
      .replace(/--/g, '-')
      .replace(/,/g, '')
      .replace(/–/g, '')
      .replace(/،/g, '')
      .replace(/#/g, '')
      .replace(/@/g, '')
      .replace(/$/g, '')
      .replace(/!/g, '')
      .replace(/&/g, '')
      .replace(/؟/g, '')
      .replace(/-{6}/g, '-')
      .replace(/-{5}/g, '-')
      .replace(/-{4}/g, '-')
      .replace(/-{3}/g, '-');
    // .replace(new RegExp('(', 'g'), '')
    // .replace(new RegExp(')', 'g'), '')
    // .replace(new RegExp('"', 'g'), '')
    // .replace(new RegExp("'", 'g'), '')
    // .replace(new RegExp(`/`, 'g'), '')
    // .replace(new RegExp('?', 'g'), '');
  }
  codeCreator() {
    if (env !== 'development' && env !== 'test') {
      const val = Math.floor(1000 + Math.random() * 8999);
      return val;
    } else {
      return 1234;
    }
  }
  checkMime(filename) {
    const i = filename.lastIndexOf('.');
    const memeType = filename.substr(i + 1);
    const imgList = ['jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'];
    const videoList = ['mp4', 'MP4'];
    const musicList = ['mp3', 'MP3'];
    const fileList = ['xls', 'pdf', 'doc', 'docx', 'apk'];
    const xlsFiles = ['xlsx'];
    if (imgList.indexOf(memeType) !== -1) {
      return 1;
    } else if (videoList.indexOf(memeType) !== -1) {
      return 3;
    } else if (fileList.indexOf(memeType) !== -1) {
      return 0;
    } else if (musicList.indexOf(memeType) !== -1) {
      return 2;
    } else if (xlsFiles.indexOf(memeType) !== -1) {
      return 4;
    }
    return 1000;
  }
  normalize(text) {
    text = text.replace(/ {5}/g, '');
    text = text.replace(/ {4}/g, '');
    text = text.replace(/ {3}/g, '');
    text = text.replace(/ {2}/g, '');
    text = text.replace(/ {1}/g, '');
    text = text.replace(/,/g, '');
    text = text.replace(/،/g, '');
    text = text.replace(/#/g, '');
    text = text.replace(/@/g, '');
    text = text.replace(/$/g, '');
    text = text.replace(/!/g, '');
    text = text.replace(/&/g, '');
    text = text.replace('(', '');
    text = text.replace(')', '');
    text = text.replace('(', '');
    text = text.replace(')', '');
    text = text.replace(/؟/g, '');
    text = text.replace('?', '');
    return text;
  }
  async sendSmsCode(phone, code) {
    // var api = Kavenegar.KavenegarApi({
    //   apikey:
    //     '39316E5954425376683938716E4957494A634164336D54472B7A47335130513054744672684A69625075553D',
    // });
    // api.Send({
    //   message: 'خدمات پیام کوتاه کاوه نگار',
    //   sender: '1000689696',
    //   receptor: '09135150939',
    // });

    const receptor = phone;
    const template = 'abk';
    const type = 'sms';
    const apiKey =
      '39316E5954425376683938716E4957494A634164336D54472B7A47335130513054744672684A69625075553D';
    const url = `https://api.kavenegar.com/v1/${apiKey}/verify/lookup.json`;
    const options = {
      url,
      method: 'POST',
      form: {
        token: code,
        receptor: receptor,
        template: template,
        type: type,
      },
    };

    const result = await smsRequest(options);

    return result;
  }
  async sendSms(phone, text) {
    return;
    const receptor = phone;
    const template = 'token';
    const type = 'sms';
    const apiKey =
      '5249504A36753150717256586244316A6A644449686D4E7072414B324638686F5953342F704A39523238413D';
    const url = `https://api.kavenegar.com/v1/${apiKey}/verify/lookup.json`;

    const options = {
      url,
      method: 'POST',
      form: {
        token: text,
        receptor: receptor,
        template: template,
        type: type,
      },
    };

    const result = await smsRequest(options);
    return result;
  }
  async sendDataChat(phone, id, name) {
    const url = `https://abk-chat.iran.liara.run/Sign-user`;
    const options = {
      url,
      method: 'POST',
      form: {
        phone: phone,
        userID: id,
        fullname: name,
      },
    };

    const result = await smsRequest(options);
    return result;
  }

  async callCode(phone, code) {
    // var api = Kavenegar.KavenegarApi({
    //   apikey:
    //     '39316E5954425376683938716E4957494A634164336D54472B7A47335130513054744672684A69625075553D',
    // });
    // api.Send({
    //   message: 'خدمات پیام کوتاه کاوه نگار',
    //   sender: '1000689696',
    //   receptor: '09135150939',
    // });

    const receptor = phone;
    const template = 'abk';
    const type = 'call';
    const apiKey =
      '39316E5954425376683938716E4957494A634164336D54472B7A47335130513054744672684A69625075553D';
    const url = `https://api.kavenegar.com/v1/${apiKey}/verify/lookup.json`;
    const options = {
      url,
      method: 'POST',
      form: {
        token: code,
        receptor: receptor,
        template: template,
        type: type,
      },
    };

    const result = await smsRequest(options);

    return result;
  }

  removeZiro(phone) {
    const tsNumberAsString = phone.toString();

    if (tsNumberAsString.charAt(0) === '0') {
      // If the first digit is zero, return the number as is
      return phone;
    } else {
      // If the first digit is not zero, add a zero in front of it
      const modifiedNumberAsString = '0' + tsNumberAsString;
      return modifiedNumberAsString;
    }

    return tsNumberAsString;
  }

  async getCurr() {
    const url = `https://sourcearena.ir/api/?token=d5c8367418ba32c3b68d5570b00cc888&currency`;
    const options = {
      url,
      method: 'GET',
    };
    const res = await smsRequest(options, async (error, response, body) => {
      return body;
      const data = JSON.parse(body);
      data.data.filter((item) => {
        if (item.slug === 'USD' || item.slug === 'SEKE_EMAMI') {
          return item;
        }
      });
    });
    return res;
  }
}
