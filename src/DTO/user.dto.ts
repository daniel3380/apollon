import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsNumberString } from 'class-validator';
import { MediaDto } from './media.dto';

export function userMidObj(user, token = null) {
  return {
    id: user.id,
    lastName: user.lastName,
    firstName: user.firstName,
    username: user.username,
    phone: user.phone,
    avatar: user.avatar,
    token,
  };
}

export function userObj(user) {
  return {
    id: user.id,
    lastName: user.lastName,
    firstName: user.firstName,
    username: user.username,
    phone: user.phone,
    avatar: user.avatar,
    token: user.token,
  };
}

export function userSmall(user) {
  return {
    id: user.id,
    lastName: user.lastName,
    firstName: user.firstName,
    avatar: user.avatar,
  };
}

export class UserDto {
  @IsNumberString()
  phone: string;

  @ApiProperty({ type: String })
  lastName: string;

  @ApiProperty({ type: String })
  firstName: string;

  @ApiProperty({ type: String })
  username: string;

  @ApiProperty({ type: String })
  token: string;

  @ApiProperty({ type: String })
  avatar: string;
}

export class LoginStepTwoDto {
  @IsNumberString()
  @ApiProperty({ type: String })
  @IsNotEmpty({ message: 'شماره موبایل اجباری می باشد' })
  phone: string;

  @IsNotEmpty({ message: ' مشکلی پیش آمده است ' })
  @ApiProperty({ type: String })
  token: string;

  @IsNotEmpty({ message: 'کد تایید اجباری می باشد' })
  @ApiProperty({ type: Number })
  code: number;
}

export class LoginDto {
  @IsNumberString()
  @ApiProperty({ type: String })
  @IsNotEmpty({ message: 'شماره موبایل اجباری می باشد' })
  phone: string;

  @ApiProperty({ type: String })
  @IsNotEmpty({ message: 'نوع تماس اجباری می باشد' })
  type: string;
}

export class LoginDtoResponse {
  @ApiProperty({ type: String })
  type: string;

  @ApiProperty({ type: Boolean })
  status: boolean;
}

export class TokenDto {
  @IsNumberString()
  @ApiProperty({ type: String })
  @IsNotEmpty({ message: 'شماره موبایل اجباری می باشد' })
  phone: string;

  @IsNumberString()
  @IsNotEmpty({ message: ' مشکلی پیش آمده است ' })
  @ApiProperty({ type: String })
  token: string;

  @IsNumberString()
  @IsNotEmpty({ message: 'کد تایید اجباری می باشد' })
  @ApiProperty({ type: String })
  code: string;
}

export class LoginBodyDto {
  @IsNumberString()
  @ApiProperty({ type: String })
  @IsNotEmpty({ message: 'شماره موبایل اجباری می باشد' })
  phone: string;

  @IsNumberString()
  @ApiProperty({ type: String })
  @IsNotEmpty({ message: 'نوع تماس اجباری می باشد' })
  type: string;
}

export class UserMidDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  mobile: string;

  @ApiProperty({ type: String })
  username: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  lastName: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  gender: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  genderCode: number;

  @ApiProperty({ type: String })
  qrCodeUrl: string;

  @IsEmail()
  @ApiProperty({ type: String })
  email: string;

  @ApiProperty({ type: String })
  token: string;

  @ApiProperty({ type: MediaDto })
  avatar: MediaDto;
}

export class AddUsernameDto {
  @ApiProperty({ type: String })
  @IsNotEmpty({ message: 'نام کاربری اجباری می باشد' })
  username: string;
}

export class AddAvatarDto {
  @ApiProperty({ type: String })
  @IsNotEmpty({ message: 'آواتار اجباری می باشد' })
  avatar: string;
}

export class NameFamilyDto {
  @ApiProperty({ type: String })
  @IsNotEmpty({ message: ' نام اجباری می باشد ' })
  firstName: string;

  @ApiProperty({ type: String })
  @IsNotEmpty({ message: ' نام خانوادگی اجباری می باشد' })
  lastName: string;
}
