import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty } from 'class-validator';
import { CityDto, cityObj } from './city.dto';

// OBJ *******************

export function stateObj(state) {
  let Cities = [];
  if (state.Cities) {
    Cities = state.Cities.map((city) => cityObj(city));
  }
  return {
    id: state.id,
    name: state.name,
    provinceId: state.provinceId,
    lat: state.lat,
    lng: state.lng,
    Cities,
  };
}
export function getState(state) {
  return state.name;
}
export function getStateName(state) {
  return {
    id: state.id,
    name: state.name,
  };
}
export function getCity(city) {
  return {
    id: city.id,
    name: city.name,
  };
}

// CLASS *******************

export class StateDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  provinceId: number;

  @ApiProperty({ type: Number })
  lat: number;

  @ApiProperty({ type: Number })
  lng: number;

  @ApiProperty({ isArray: true, type: CityDto })
  Cities: CityDto[];
}
export class CreatestateDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;
}
export class GetStateNameDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;
}
export class UpdatestateDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;
}
export class StateNameDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;

  @ApiProperty({ type: Number })
  lat: number;

  @ApiProperty({ type: Number })
  lng: number;
}
