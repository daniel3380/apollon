import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import commentStatuses from '../common/eNums/commentStatuses.enum';
import { postTitleObjDto } from './post.dto';

export function postCommentObj(postComment) {
  const active = Object.keys(commentStatuses).find(
    (key) => commentStatuses[key].code === postComment.active,
  );
  return {
    id: postComment.id,
    postId: postComment.postId,
    parentId: postComment.parentId,
    userName: postComment.userName,
    text: postComment.text,
    active: postComment.active,
    activeText: commentStatuses[active].text,
    createdAt: postComment.createdAt,
  };
}

export function postCommentInfoObj(postComment) {
  const active = Object.keys(commentStatuses).find(
    (key) => commentStatuses[key].code === postComment.active,
  );
  let replies = [];
  if (postComment.PostComments && postComment.PostComments.length !== 0) {
    replies = postComment.PostComments.map((postComment) =>
      postCommentObj(postComment),
    );
  }
  const post = postTitleObjDto(postComment.Post);
  return {
    id: postComment.id,
    postId: postComment.postId,
    post: post,
    parentId: postComment.parentId,
    userName: postComment.userName,
    text: postComment.text,
    active: postComment.active,
    activeText: commentStatuses[active].text,
    createdAt: postComment.createdAt,
    replies,
  };
}

export function postCommentArrObjDto(postComment) {
  const active = Object.keys(commentStatuses).find(
    (key) => commentStatuses[key].code === postComment.active,
  );
  const post = postTitleObjDto(postComment.Post);
  return {
    id: postComment.id,
    postId: postComment.postId,
    post: post.title,
    parentId: postComment.parentId,
    userName: postComment.userName,
    active: postComment.active,
    text: postComment.text,
    activeText: commentStatuses[active].text,
    createdAt: postComment.createdAt,
  };
}

export class ListPostCommentDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  postId: number;

  @ApiProperty({ type: String })
  post: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  parentId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  userName: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  active: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  activeText: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  text: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  createdAt: string;
}

export class FilterListPostCommentDto {
  @IsOptional()
  @ApiProperty({ type: Number, required: false })
  postId: number;

  @IsOptional()
  @ApiProperty({ type: Number, required: false })
  active: number;

  @IsOptional()
  @ApiProperty({ type: Date, required: false })
  fromDate: Date;

  @IsOptional()
  @ApiProperty({ type: Date, required: false })
  toDate: Date;
}

export class PostCommentDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  postId: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  parentId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  userName: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  text: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  active: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  activeText: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  createdAt: string;
}

export class PostCommentInfoDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  postId: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  parentId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  userName: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  text: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  active: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  activeText: string;

  @ApiProperty({ isArray: true, type: PostCommentDto })
  replies: PostCommentDto[];
}

export class CreatePostCommentDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  text: string;
}

export class StatusPostCommentDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  active: number;
}
