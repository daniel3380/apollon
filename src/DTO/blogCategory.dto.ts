import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsOptional } from 'class-validator';
import blogStatuses from '../common/eNums/blogStatuses.enum';
import { PostDto, postObjDto } from './post.dto';
import { CategoryDataAccess } from '../dataAccess/blogCategory.dataAccess';

const categoryDataAccess = new CategoryDataAccess();
async function findParent(id) {
  const parent = await categoryDataAccess.findById(id);
  return parent.title;
}

export async function blogCatObjDto(category) {
  const active = Object.keys(blogStatuses).find(
    (key) => blogStatuses[key].code === category.active,
  );
  let Posts = [];
  if (category.Posts && category.Posts.length !== 0) {
    Posts = category.Posts.map((post) => postObjDto(post));
  }
  const parent =
    category.parentId !== 0 ? await findParent(category.parentId) : 'root';
  return {
    id: category.id,
    parentId: category.parentId,
    parent: parent,
    title: category.title,
    description: category.description,
    activeText: blogStatuses[active].text,
    active: category.active,
    createdAt: category.createdAt,
    Posts,
  };
}

export async function blogCatArrObjDto(category) {
  const active = Object.keys(blogStatuses).find(
    (key) => blogStatuses[key].code === category.active,
  );
  const parent =
    category.parentId !== 0 ? await findParent(category.parentId) : 'root';
  return {
    id: category.id,
    parentId: category.parentId,
    parent: parent,
    title: category.title,
    description: category.description,
    active: category.active,
    activeText: blogStatuses[active].text,
    createdAt: category.createdAt,
  };
}

export function blogCatObjInfoDto(category) {
  const active = Object.keys(blogStatuses).find(
    (key) => blogStatuses[key].code === category.active,
  );
  return {
    title: category.title,
    activeText: blogStatuses[active].text,
  };
}

export function childBlogCatObj(category) {
  return {
    id: category.id,
    title: category.title,
  };
}

export function getAllBlogCategoriesObj(category) {
  let BlogCategories = [];
  if (category.BlogCategories && category.BlogCategories !== 0) {
    BlogCategories = category.BlogCategories.map((category) =>
      childBlogCatObj(category),
    );
  }
  return {
    id: category.id,
    title: category.title,
    BlogCategories,
  };
}

export class BlogCatInfoDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  activeText: string;
}

export class ChildBlogCatDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;
}

export class GetAllBlogCategoriesDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({ type: ChildBlogCatDto, isArray: true })
  BlogCategories: ChildBlogCatDto[];
}

export class BlogCatDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  parentId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  parent: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({ type: String })
  description: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  active: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  activeText: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  createdAt: string;

  @ApiProperty({ type: PostDto, isArray: true })
  Posts: PostDto[];
}

export class BlogCatListDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  parentId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  parent: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({ type: String })
  description: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  active: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  activeText: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  createdAt: string;
}

export class CreateBlogCatDto {
  @ApiProperty({ type: Number })
  parentId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({ type: String })
  description: string;
}

export class UpdateBlogCatDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  id: number;

  @ApiProperty({ type: Number })
  parentId: number;

  @IsNotEmpty()
  @IsOptional()
  @ApiProperty({ type: String })
  title: string;

  @IsOptional()
  @ApiProperty({ type: String })
  description: string;
}

export class StatusBlogCatDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  active: number;
}

export class getParentListBlogCatDto {
  @IsOptional()
  @ApiProperty({ type: Number, required: false })
  parentId: number;
}
