import blogStatuses from '../common/eNums/blogStatuses.enum';
import * as Models from '../models/index';

export class CategoryDataAccess {
  async checkDuplicate(title) {
    const category = await Models.BlogCategory.findOne({
      where: {
        title: title,
      },
    });
    return category;
  }

  async createCategory(parentId, title, description) {
    const category = await Models.BlogCategory.create({
      parentId,
      title,
      description,
    });
    return category;
  }

  async findAllBlogCategries(parentId) {
    const categories = await Models.BlogCategory.findAll({
      where: { parentId: parentId },
      order: [['id', 'DESC']],
    });
    return categories;
  }

  async findAll(parentId) {
    const where: any = {};
    if (parentId) {
      where.parentId = parseInt(parentId);
    }
    const categories = await Models.BlogCategory.findAll({
      where,
      include: [Models.BlogCategory],
      order: [['id', 'DESC']],
    });
    return categories;
  }

  async findById(id) {
    const category = await Models.BlogCategory.findOne({
      where: {
        id,
      },
      include: [
        Models.BlogCategory,
        { model: Models.Post, include: [Models.BlogCategory, Models.Media] },
      ],
    });
    return category;
  }

  async updateBlogCategory(id, parentId, title, description) {
    await Models.BlogCategory.update(
      {
        parentId,
        title,
        description,
        updatedAt: new Date(),
      },
      {
        where: {
          id,
        },
      },
    );
  }

  async changeStatusCategory(id, active) {
    await Models.BlogCategory.update(
      {
        active,
      },
      {
        where: {
          id,
        },
      },
    );
  }

  async getAll() {
    const list = await Models.BlogCategory.findAll({
      where: {
        parentId: 0,
        active: blogStatuses.active.code,
      },
      include: [Models.BlogCategory],
    });
    return list;
  }
}
