import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { MediaDto, mediaObj } from './media.dto';
import { postTitleObjDto } from './post.dto';

export function relatedPostObjDto(relatedPost) {
  const media = mediaObj(relatedPost.Media);
  let post = null;
  if (relatedPost.postId !== null) {
    const postTitle = postTitleObjDto(relatedPost.Post);
    post = postTitle.title;
  }
  return {
    id: relatedPost.id,
    postId: relatedPost.postId,
    post: post,
    mediaId: relatedPost.mediaId,
    media: media,
    title: relatedPost.title,
    url: relatedPost.url,
    description: relatedPost.description,
    createdAt: relatedPost.createdAt,
  };
}

export class RelatedPostDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @ApiProperty({ type: Number })
  postId: number;

  @ApiProperty({ type: String })
  post: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  mediaId: number;

  @IsNotEmpty()
  @ApiProperty({ type: MediaDto })
  media: MediaDto;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  url: string;

  @ApiProperty({ type: String })
  description: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  createdAt: string;
}

export class CreateRelatedPostDto {
  @ApiProperty({ type: Number })
  postId: number;

  @ApiProperty({ type: Number })
  mediaId: number;

  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({ type: String })
  url: string;

  @ApiProperty({ type: String })
  description: string;
}

export class UpdatedRelatedPost {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @ApiProperty({ type: Number })
  postId: number;

  @ApiProperty({ type: Number })
  mediaId: number;

  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({ type: String })
  url: string;

  @ApiProperty({ type: String })
  description: string;
}
