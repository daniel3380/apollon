import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsNumberString, IsOptional } from 'class-validator';
import blogStatuses from '../common/eNums/blogStatuses.enum';
import {
  PostMediaDto,
  postMediaObj,
  CreatePostMediaDto,
} from './postMedia.dto';
import { PostCommentDto, postCommentObj } from './postComment.dto';
import { MediaDto, mediaObj } from './media.dto';
import { BlogCatInfoDto, blogCatObjInfoDto } from './blogCategory.dto';
import { adminObjDto, AdminInfoDto } from './admin.dto';
import { Optional } from '@nestjs/common';
import { relatedPostObjDto, RelatedPostDto } from './relatedPost.dto';

export function postObjDto(post) {
  const status = Object.keys(blogStatuses).find(
    (key) => blogStatuses[key].code === post.active,
  );
  const blogCat = blogCatObjInfoDto(post.BlogCategory);
  return {
    id: post.id,
    adminId: post.adminId,
    blogCatId: post.blogCatId,
    blogCat,
    title: post.title,
    slug: post.slug,
    visitCount: post.visitCount,
    activeText: blogStatuses[status].text,
    active: post.active,
    createdAt: post.createdAt,
  };
}

export function viewPostObjDto(post) {
  const blogCat = blogCatObjInfoDto(post.BlogCategory);
  const media = post.Media ? mediaObj(post.Media) : null;
  return {
    id: post.id,
    blogCatId: post.blogCatId,
    blogCat,
    mediaId: post.mediaId,
    media,
    title: post.title,
    slug: post.slug,
    description: post.description,
    visitCount: post.visitCount,
    createdAt: post.createdAt,
  };
}

export function landingResultObjDto(posts) {
  let resultPosts = [];
  if (posts && posts.length !== 0) {
    resultPosts = posts.map((post) => viewPostObjDto(post));
  }

  return {
    resultPosts,
  };
}

export function postInfoObjDto(post) {
  const status = Object.keys(blogStatuses).find(
    (key) => blogStatuses[key].code === post.active,
  );

  let PostMedias = [];
  if (post.PostMedias && post.PostMedias.length !== 0) {
    PostMedias = post.PostMedias.map((postMedia) => postMediaObj(postMedia));
  }
  let PostComments = [];
  if (post.PostComments && post.PostComments.length !== 0) {
    PostComments = post.PostComments.map((postComment) =>
      postCommentObj(postComment),
    );
  }
  let RelatedPosts = [];
  if (post.RelatedPosts && post.RelatedPosts.length !== 0) {
    RelatedPosts = post.RelatedPosts.map((RelatedPost) =>
      relatedPostObjDto(RelatedPost),
    );
  }
  const blogCat = blogCatObjInfoDto(post.BlogCategory);
  const admin = adminObjDto(post.Admin);
  let media = null;
  if (post.Media) {
    media = mediaObj(post.Media);
  }
  return {
    id: post.id,
    adminId: post.adminId,
    admin,
    blogCatId: post.blogCatId,
    blogCat,
    mediaId: post.mediaId,
    media,
    description: post.description,
    title: post.title,
    slug: post.slug,
    text: post.text,
    writer: post.writer,
    mediaAlt: post.mediaAlt,
    metaTitle: post.metaTitle,
    metaDescription: post.metaDescription,
    metaKeywords: post.metaKeywords,
    visitCount: post.visitCount,
    sort: post.sort,
    isInLanding: post.isInLanding,
    activeText: blogStatuses[status].text,
    active: post.active,
    createdAt: post.createdAt,
    PostMedias,
    PostComments,
    RelatedPosts,
  };
}

export function postTitleObjDto(post) {
  const status = Object.keys(blogStatuses).find(
    (key) => blogStatuses[key].code === post.active,
  );
  return {
    id: post.id,
    title: post.title,
    slug: post.slug,
    activeText: blogStatuses[status].text,
  };
}

export class PostDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  blogCatId: number;

  @ApiProperty({ type: () => BlogCatInfoDto })
  blogCat: BlogCatInfoDto;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  adminId: number;

  @ApiProperty({ type: AdminInfoDto })
  admin: AdminInfoDto;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  mediaId: number;

  @ApiProperty({ type: MediaDto })
  media: MediaDto;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  description: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  slug: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  text: string;

  @ApiProperty({ type: String })
  writer: string;

  @ApiProperty({ type: String })
  mediaAlt: string;

  @ApiProperty({ type: String })
  metaTitle: string;

  @ApiProperty({ type: String })
  metaDescription: string;

  @ApiProperty({ type: String })
  metaKeywords: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  visitCount: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  activeText: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  active: number;

  @ApiProperty({ type: Number })
  sort: number;

  @IsNotEmpty()
  @ApiProperty({ type: Boolean })
  isInLanding: boolean;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  createdAt: string;

  @ApiProperty({ isArray: true, type: PostMediaDto })
  PostMedias: PostMediaDto[];

  @ApiProperty({ isArray: true, type: PostCommentDto })
  PostComments: PostCommentDto[];

  @ApiProperty({ isArray: true, type: RelatedPostDto })
  RelatedPosts: RelatedPostDto[];
}

export class ListPostDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  blogCatId: number;

  @ApiProperty({ type: () => BlogCatInfoDto })
  blogCat: BlogCatInfoDto;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  adminId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  slug: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  visitCount: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  active: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  activeText: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  createdAt: string;
}

export class ViewListPostDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  blogCatId: number;

  @ApiProperty({ type: () => BlogCatInfoDto })
  blogCat: BlogCatInfoDto;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  mediaId: number;

  @ApiProperty({ type: MediaDto })
  media: MediaDto;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  slug: string;

  @ApiProperty({ type: String })
  description: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  visitCount: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  createdAt: string;
}

export class FilterListPostDto {
  @IsOptional()
  @ApiProperty({ type: Number, required: false })
  blogCatId: number;

  @IsOptional()
  @ApiProperty({ type: String, required: false })
  title: string;

  @IsOptional()
  @ApiProperty({ type: String, required: false })
  slug: string;

  @IsOptional()
  @ApiProperty({ type: Number, required: false })
  visitCount: number;

  @IsOptional()
  @ApiProperty({ type: Boolean, required: false })
  isInLanding: boolean;

  @IsOptional()
  @ApiProperty({ type: Number, required: false })
  active: number;
}

export class CreatePostDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  blogCatId: number;

  @ApiProperty({ type: Number })
  mediaId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  description: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  text: string;

  @ApiProperty({ type: String })
  writer: string;

  @ApiProperty({ type: String })
  mediaAlt: string;

  @ApiProperty({ type: String })
  metaTitle: string;

  @ApiProperty({ type: String })
  metaDescription: string;

  @ApiProperty({ type: String })
  metaKeywords: string;

  @IsNotEmpty()
  @ApiProperty({ type: Boolean })
  isInLanding: boolean;

  @Optional()
  @ApiProperty({ isArray: true, type: CreatePostMediaDto })
  postMedias: CreatePostMediaDto[];
}

export class UpdatePostDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  blogCatId: number;

  @IsInt()
  @ApiProperty({ type: Number })
  mediaId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  description: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  text: string;

  @ApiProperty({ type: String })
  writer: string;

  @ApiProperty({ type: String })
  mediaAlt: string;

  @ApiProperty({ type: String })
  metaTitle: string;

  @ApiProperty({ type: String })
  metaDescription: string;

  @ApiProperty({ type: String })
  metaKeywords: string;

  @IsNotEmpty()
  @ApiProperty({ type: Boolean })
  isInLanding: boolean;

  @Optional()
  @ApiProperty({ isArray: true, type: CreatePostMediaDto })
  postMedias: CreatePostMediaDto[];
}

export class PostStatusDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  active: number;
}

export class ChangeSortPostDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  fromId: number;

  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  toId: number;
}

export class getPostInputDto {
  @IsOptional()
  @IsNumberString()
  @ApiProperty({ type: Number, required: false })
  blogCatId: number;

  @IsOptional()
  @ApiProperty({ type: Number, required: false })
  page: number;

  @IsOptional()
  @ApiProperty({ type: Number, required: false })
  pageSize: number;
}

export class LandingResultDto {
  @ApiProperty({ isArray: true, type: ViewListPostDto })
  resultPosts: ViewListPostDto[];
}

export class SearchParametr {
  @IsOptional()
  @ApiProperty({ type: String, required: false })
  title: string;
}
