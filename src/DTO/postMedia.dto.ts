import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumberString } from 'class-validator';
import { mediaObj, MediaDto } from './media.dto';

export function postMediaObj(postMedia) {
  return {
    id: postMedia.id,
    mediaId: postMedia.mediaId,
    title: postMedia.title,
    mediaObj: mediaObj(postMedia.Media),
  };
}

export class PostMediaDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  mediaId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({
    type: MediaDto,
  })
  mediaObj: MediaDto;
}

export class CreatePostMediaDto {
  @IsNotEmpty()
  @IsNumberString()
  @ApiProperty({ type: Number })
  mediaId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;
}

export class DeletePostMediaDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  postId: number;
}
