import {
  Column,
  Table,
  Model,
  ForeignKey,
  BelongsTo,
  HasMany,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { BlogCategory } from './blogCategory.model';
import { Admin } from './admin.model';
import { PostMedia } from './postMedia.model';
import { Media } from './media.model';
import { PostComment } from './postComment.model';
import { RelatedPost } from './relatedPost.model';

@Table({
  tableName: 'posts',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class Post extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @ForeignKey(() => BlogCategory)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'blog_categories', key: 'id' },
  })
  blogCatId: number;

  @ForeignKey(() => Admin)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'admins', key: 'id' },
  })
  adminId: number;

  @ForeignKey(() => Media)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'medias', key: 'id' },
  })
  mediaId: number;

  @Column({
    allowNull: false,
    type: Sequelize.TEXT,
  })
  description: string;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  title: string;

  @Column({
    allowNull: false,
    unique: true,
    type: Sequelize.STRING,
  })
  slug: string;

  @Column({
    allowNull: false,
    type: Sequelize.TEXT,
  })
  text: string;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  writer: string;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  mediaAlt: string;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  metaTitle: string;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  metaDescription: string;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  metaKeywords: string;

  @Column({
    defaultValue: 0,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  visitCount: number;

  @Column({
    defaultValue: 0,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  active: number;

  @Column({
    defaultValue: 0,
    allowNull: true,
    type: Sequelize.INTEGER,
  })
  sort: number;

  @Column({
    defaultValue: false,
    allowNull: false,
    type: Sequelize.BOOLEAN,
  })
  isInLanding: boolean;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => Admin, { foreignKey: 'adminId' })
  Admin: Admin;

  @BelongsTo(() => BlogCategory, { foreignKey: 'blogCatId' })
  BlogCategory: BlogCategory;

  @BelongsTo(() => Media, { foreignKey: 'mediaId' })
  Media: Media;

  @HasMany(() => PostComment, { foreignKey: 'postId' })
  PostComments: PostComment[];

  @HasMany(() => PostMedia, { foreignKey: 'postId' })
  PostMedias: PostMedia[];

  @HasMany(() => RelatedPost, { foreignKey: 'postId' })
  RelatedPosts: RelatedPost[];
}
