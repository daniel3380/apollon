import {
  Column,
  Table,
  Model,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { Post } from './post.model';
import { Media } from './media.model';

@Table({
  tableName: 'related_posts',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class RelatedPost extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @ForeignKey(() => Post)
  @Column({
    allowNull: true,
    type: Sequelize.INTEGER,
    references: { model: 'posts', key: 'id' },
  })
  postId: number;

  @ForeignKey(() => Media)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'medias', key: 'id' },
  })
  mediaId: number;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  title: string;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  url: string;

  @Column({
    allowNull: true,
    type: Sequelize.TEXT,
  })
  description: string;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => Post, { foreignKey: 'postId' })
  Post: Post;

  @BelongsTo(() => Media, { foreignKey: 'mediaId' })
  Media: Media;
}
