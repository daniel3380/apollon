import { Column, Table, Model, HasMany } from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { Post } from './post.model';

@Table({
  tableName: 'blog_categories',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class BlogCategory extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    defaultValue: 0,
    allowNull: true,
    type: Sequelize.INTEGER,
  })
  parentId: number;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  title: string;

  @Column({
    allowNull: true,
    type: Sequelize.TEXT,
  })
  description: string;

  @Column({
    defaultValue: 0,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  active: number;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @HasMany(() => Post, { foreignKey: 'blogCatId' })
  Posts: Post[];

  @HasMany(() => BlogCategory, { constraints: false, foreignKey: 'parentId' })
  BlogCategories: BlogCategory[];
}
