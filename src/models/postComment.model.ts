import {
  Column,
  Table,
  Model,
  ForeignKey,
  BelongsTo,
  HasMany,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { Post } from './post.model';
import commentStatuses from '../common/eNums/commentStatuses.enum';

@Table({
  tableName: 'post_comments',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class PostComment extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @ForeignKey(() => Post)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'posts', key: 'id' },
  })
  postId: number;

  @Column({
    defaultValue: 0,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  parentId: number;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  userName: string;

  @Column({
    allowNull: false,
    type: Sequelize.TEXT,
  })
  text: string;

  @Column({
    defaultValue: commentStatuses.published.code,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  active: number;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => Post, { foreignKey: 'postId' })
  Post: Post;

  @HasMany(() => PostComment, { constraints: false, foreignKey: 'parentId' })
  PostComments: PostComment[];
}
