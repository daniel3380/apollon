import Sequelize from 'sequelize';
import { Column, Model, Table } from 'sequelize-typescript';

@Table({
  tableName: 'medias',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class Media extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    allowNull: true,
    type: Sequelize.INTEGER,
    references: { model: 'users', key: 'id' },
  })
  userId: number;

  @Column({
    type: Sequelize.STRING,
    allowNull: false,
  })
  mediaUrl: string;

  @Column({
    allowNull: true,
    type: Sequelize.INTEGER,
  })
  ownerId: number;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  ownerModel: string;

  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  mimeType: number;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;
}
