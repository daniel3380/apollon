import Sequelize from 'sequelize';
import { Column, HasMany, Model, Table } from 'sequelize-typescript';

import { City } from './city.model';
import { User } from './user.model';

@Table({
  tableName: 'provinces',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class Province extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    type: Sequelize.STRING,
    allowNull: false,
  })
  name: string;

  @Column({
    type: Sequelize.FLOAT,
    allowNull: true,
  })
  lat: number;

  @Column({
    allowNull: true,
    type: Sequelize.FLOAT,
  })
  lng: number;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @HasMany(() => City, { foreignKey: 'provinceId' })
  Cities: City[];

  @HasMany(() => User, { foreignKey: 'provinceId' })
  Users: User[];
}
