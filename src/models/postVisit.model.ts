import {
  Column,
  Table,
  Model,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { User } from './user.model';
import { Post } from './post.model';

@Table({
  tableName: 'post_visits',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class PostVisit extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @ForeignKey(() => User)
  @Column({
    allowNull: true,
    type: Sequelize.INTEGER,
    references: { model: 'users', key: 'id' },
  })
  userId: number;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  ip: string;

  @ForeignKey(() => Post)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'posts', key: 'id' },
  })
  postId: number;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => User, { foreignKey: 'userId' })
  User: User;

  @BelongsTo(() => Post, { foreignKey: 'postId' })
  Post: Post;
}
