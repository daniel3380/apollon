import * as Models from '../models/index';
export class PostVisitDataAccess {
  async findById(userId, postId, ip) {
    const post = await Models.PostVisit.findOne({
      where: {
        postId,
        userId,
        $or: {
          ip,
        },
      },
    });
    return post;
  }
  async create(userId, postId, ip) {
    const post = await Models.PostVisit.create({
      userId,
      ip,
      postId,
    });
    return post;
  }
}
