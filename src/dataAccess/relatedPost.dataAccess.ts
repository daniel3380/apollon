import * as Models from '../models/index';

export class RelatedPostDataAccess {
  async createRequest(postId, mediaId, title, url, description) {
    const item = await Models.RelatedPost.create({
      postId,
      mediaId,
      title,
      url,
      description,
    });
    return item;
  }

  async findById(id) {
    const item = await Models.RelatedPost.findOne({
      where: {
        id,
      },
      include: [Models.Post, Models.Media],
    });
    return item;
  }

  async updateRequest(id, postId, mediaId, title, url, description) {
    await Models.RelatedPost.update(
      {
        postId,
        mediaId,
        title,
        url,
        description,
        updatedAt: new Date(),
      },
      {
        where: {
          id,
        },
      },
    );
  }

  async deleteRequest(id) {
    await Models.RelatedPost.destroy({
      where: { id },
    });
    return;
  }
}
