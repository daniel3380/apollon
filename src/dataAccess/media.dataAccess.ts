import * as Models from '../models/index';

export class MediaDataAccess {
  async findById(id) {
    const media = await Models.Media.findByPk(id);
    return media;
  }
  async createMedia(mediaUrl, mimeType) {
    const media = await Models.Media.create({
      mediaUrl,
      mimeType,
      ownerId: 0,
      ownerModel: '',
    });

    return media;
  }
  async createMediaCloud(mediaUrl, userId) {
    const media = await Models.Media.create({
      mediaUrl,
      userId,
      mimeType: '1',
      ownerId: 0,
      ownerModel: '',
    });

    return media;
  }
  async updateMediaOwner(ownerId, ownerModel, id) {
    await Models.Media.update(
      {
        ownerId,
        ownerModel,
        updatedAt: new Date(),
      },
      {
        where: {
          id,
        },
      },
    );
  }
  async deleteMedia(id) {
    await Models.Media.destroy({
      where: { id },
    });
    return;
  }
}
