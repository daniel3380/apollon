import * as Models from '../models/index';

export class CityDataAccess {
  async findStateById(id) {
    const state = await Models.Province.findByPk(id);
    return state;
  }
  async findCityById(id) {
    const city = await Models.City.findOne({
      where: {
        id: id,
      },
    });
    return city;
  }
  async findCityParentById(id, provinceId) {
    const city = await Models.City.findOne({
      where: {
        id: id,
        provinceId,
      },
    });
    return city;
  }
  async create(name, provinceId, lat, lng) {
    const state = await Models.City.create({ provinceId, name, lat, lng });
    return state;
  }
}
