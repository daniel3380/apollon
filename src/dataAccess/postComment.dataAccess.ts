import * as Models from '../models/index';

export class PostCommentDataAccess {
  async createPostComment(postId, userName, text) {
    const postComment = await Models.PostComment.create({
      postId,
      userName,
      text,
    });
    return postComment;
  }

  async replyComment(postId, parentId, userName, text) {
    const postComment = await Models.PostComment.create({
      postId,
      parentId,
      userName,
      text,
    });
    return postComment;
  }

  async findById(id) {
    const item = await Models.PostComment.findOne({
      where: {
        id,
      },
      include: [
        Models.Post,
        { model: Models.PostComment, include: [Models.Post] },
      ],
    });
    return item;
  }

  async changeStatus(id, active) {
    await Models.PostComment.update(
      {
        active,
      },
      {
        where: {
          id,
        },
      },
    );
  }

  async listAndFilter(filterListPostCommentDto) {
    const where: any = {};
    const { postId, active, fromDate, toDate } = filterListPostCommentDto;
    if (postId) {
      where.postId = postId;
    }
    if (active) {
      where.active = active;
    }
    const from3mounts = new Date();
    from3mounts.setDate(from3mounts.getDate() - 90);
    const fromDateWhereC = fromDate || from3mounts;

    const toDateWhereC = toDate || new Date();
    where.createdAt = {
      $between: [fromDateWhereC, toDateWhereC],
    };
    where.parentId = 0;
    const list = await Models.PostComment.findAll({
      where,
      order: [['id', 'DESC']],
      limit: 500,
      include: [Models.Post],
    });
    return list;
  }
}
