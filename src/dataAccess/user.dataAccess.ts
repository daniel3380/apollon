import * as Models from '../models/index';

export class UserDataAccess {
  async findUserToken(jwtToken) {
    const token = await Models.User.findOne({
      where: {
        token: jwtToken,
      },
    });
    return token;
  }

  async findById(id) {
    const user = await Models.User.findByPk(id, {
      // include: [Models.Media],
    });
    return user;
  }

  async findOne(phoneClean) {
    const result = await Models.User.findOne({
      where: { phone: phoneClean },
    });
    return result;
  }

  async findUsername(username) {
    const result = await Models.User.findOne({
      where: { username },
    });
    return result;
  }

  async findOneId(userId) {
    const result = await Models.User.findOne({
      where: { id: userId },
    });
    return result;
  }

  async findUserAroundMe(userId) {
    const result = await Models.User.findAll({
      where: { id: userId },
    });
    return result;
  }

  async avatar(avatar, userId) {
    const result = await Models.User.update(
      { avatar },
      {
        where: { id: userId },
      },
    );
    return result;
  }

  async create(phoneClean) {
    const result = await Models.User.create({
      phone: phoneClean,
    });
    return result;
  }

  async update(userId, jwt) {
    const result = await Models.User.update(
      {
        token: jwt,
      },
      {
        where: {
          id: userId,
        },
      },
    );
    return result;
  }

  async addNameFamily(id, firstName, lastName) {
    const result = await Models.User.update(
      {
        firstName,
        lastName,
      },
      {
        where: {
          id,
        },
      },
    );

    return result;
  }

  async updateUsername(userId, username) {
    const result = await Models.User.update(
      {
        username,
      },
      {
        where: {
          id: userId,
        },
      },
    );
    return result;
  }

  async userFaker(firstName, lastName, username, email, avatar, lat, lon) {
    const smsRq = await Models.User.create({
      firstName,
      lastName,
      username,
      email,
      avatar,
      lat,
      lon,
    });
    return smsRq;
  }
}
