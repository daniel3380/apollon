import * as Models from '../models/index';

export class SmsRqDataAccess {
  async findSmsRq(mobile, ip) {
    const smsRq = await Models.SmsRq.findOne({
      where: {
        $or: {
          ip,
          mobile,
        },
      },
    });
    return smsRq;
  }
  async updateSmsRq(id, jwtToken, mobile, sendCount, ip, code) {
    await Models.SmsRq.update(
      {
        jwtToken,
        mobile,
        sendCount,
        ip,
        code,
        lastSentTime: new Date(),
      },
      {
        where: {
          id,
        },
      },
    );
    // );
    // await this.smsRq
    //   .createQueryBuilder()
    //   .update()
    //   .set({ jwtToken, mobile, sendCount, ip, code, lastSentTime: new Date() })
    //   .where({ id }).execute;
  }
  async createSmsRq(jwtToken, mobile, ip, code) {
    await Models.SmsRq.create({
      jwtToken,
      mobile,
      sendCount: 1,
      ip,
      code,
      lastSentTime: new Date(),
    });
  }

  deleteSmsRqs() {
    // this.smsRq.destroy({
    //   where: {
    //     lastSentTime: {
    //       $lte: date,
    //     },
    //   },
    // });

    const date = new Date();
    date.setDate(date.getDate() - 1);
    Models.SmsRq.destroy({
      where: {
        lastSentTime: {
          $lte: date,
        },
      },
    });
  }
  deleteSmsRq(id) {
    Models.SmsRq.destroy({
      where: {
        id,
      },
    });
  }
  async validateCode(mobile, code, jwtToken) {
    const smsRq = await Models.SmsRq.findOne({
      where: {
        mobile,
        code,
        jwtToken,
      },
    });
    return smsRq;
  }
}
