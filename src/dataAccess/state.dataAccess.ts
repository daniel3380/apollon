import * as Models from '../models/index';

export class StateDataAccess {
  async findAll(): Promise<Models.Province[]> {
    const provinces = await Models.Province.findAll({
      include: [Models.City],
      order: [['id', 'ASC']],
    });
    return provinces;
  }
  async findNearestCities(lat, lng): Promise<Models.City[]> {
    const cities = await Models.City.findAll({
      where: {
        lat: { $between: [lat - 1, lat + 1] },
        lng: { $between: [lng - 1, lng + 1] },
      },
    });
    return cities;
  }
  async findCityById(id): Promise<Models.City> {
    const city = await Models.City.findByPk(id);
    return city;
  }
  async updateCityLocation(id, lat, lng): Promise<Models.City> {
    await Models.City.update(
      {
        lat,
        lng,
      },
      {
        where: {
          id,
        },
      },
    );
    return await this.findCityById(id);
  }
}
