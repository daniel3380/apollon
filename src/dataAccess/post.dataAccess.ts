import blogStatuses from 'src/common/eNums/blogStatuses.enum';
import * as Models from '../models/index';

export class PostDataAccess {
  async checkDuplicate(slug) {
    const post = await Models.Post.findOne({
      where: {
        slug: slug,
      },
    });
    return post;
  }

  async checkTitle(title) {
    const post = await Models.Post.findOne({
      where: {
        title: title,
      },
    });
    return post;
  }

  async createPostRequest(
    adminId,
    blogCatId,
    mediaId,
    title,
    description,
    slug,
    text,
    writer,
    mediaAlt,
    metaTitle,
    metaDescription,
    metaKeywords,
    isInLanding,
  ) {
    const post = await Models.Post.create({
      blogCatId,
      adminId,
      mediaId,
      title,
      description,
      slug,
      text,
      writer,
      mediaAlt,
      metaTitle,
      metaDescription,
      metaKeywords,
      isInLanding,
    });
    return post;
  }

  async updateSortPost(id) {
    await Models.Post.update(
      {
        sort: id,
      },
      {
        where: {
          id,
        },
      },
    );
  }

  async findPostBySlug(slug) {
    const post = await Models.Post.findOne({
      where: {
        slug: slug,
        active: blogStatuses.active.code,
      },
      include: [
        Models.Media,
        Models.BlogCategory,
        Models.Admin,
        { model: Models.PostComment, include: [Models.Post] },
        { model: Models.RelatedPost, include: [Models.Media, Models.Post] },
        { model: Models.PostMedia, include: [Models.Media] },
      ],
    });
    return post;
  }

  async findById(id) {
    const post = await Models.Post.findOne({
      where: {
        id,
      },
      include: [
        Models.Media,
        Models.BlogCategory,
        Models.Admin,
        { model: Models.PostComment, include: [Models.Post] },
        { model: Models.RelatedPost, include: [Models.Media, Models.Post] },
        { model: Models.PostMedia, include: [Models.Media] },
      ],
    });
    return post;
  }

  // eslint-disable-next-line prettier/prettier
  async updatePost(
    id,
    adminId,
    blogCatId,
    mediaId,
    title,
    description,
    text,
    writer,
    mediaAlt,
    metaTitle,
    metaDescription,
    metaKeywords,
    isInLanding,
  ) {
    await Models.Post.update(
      {
        adminId,
        blogCatId,
        mediaId,
        title,
        description,
        text,
        writer,
        mediaAlt,
        metaTitle,
        metaDescription,
        metaKeywords,
        isInLanding,
        active: 0,
        updatedAt: new Date(),
      },
      {
        where: {
          id,
        },
      },
    );
  }

  async changeStatusPost(id, active) {
    await Models.Post.update(
      {
        active,
        updatedAt: new Date(),
      },
      {
        where: {
          id,
        },
      },
    );
  }

  async filterlistAndFilter(filterListPostDto) {
    const where: any = {};
    const { blogCatId, title, visitCount, active, isInLanding } =
      filterListPostDto;
    if (title && title.length > 2) {
      where.title = { $like: `%${title}%` };
    }
    if (blogCatId) {
      where.blogCatId = blogCatId;
    }
    if (visitCount) {
      where.visitCount = visitCount;
    }
    if (active) {
      where.active = active;
    }
    if (isInLanding) {
      where.isInLanding = isInLanding;
    }
    const posts = await Models.Post.findAll({
      where,
      order: [['sort', 'DESC']],
      include: [Models.BlogCategory, Models.Media],
    });
    return posts;
  }

  async changeSortPost(fromId, toId) {
    await Models.Post.findOne({
      where: {
        id: fromId,
      },
    }).then((fromRow) => {
      Models.Post.findOne({
        where: {
          id: toId,
        },
      }).then((toRow) => {
        const toSort = fromRow.sort;
        const toFrom = toRow.sort;
        Models.Post.update(
          {
            sort: toSort,
          },
          {
            where: {
              id: toId,
            },
          },
        ).then(() => {
          Models.Post.update(
            {
              sort: toFrom,
            },
            {
              where: {
                id: fromId,
              },
            },
          );
        });
      });
    });
  }

  async updateVisitCount(id, visitCount) {
    await Models.Post.update(
      {
        visitCount,
      },
      {
        where: {
          id,
        },
      },
    );
  }

  async listPosts(query) {
    const where: any = {};
    const { blogCatId, page, pageSize } = query;
    if (blogCatId) {
      where.blogCatId = blogCatId;
    }
    where.active = blogStatuses.active.code;
    const limit = parseInt(pageSize) || 12;
    const offset = (parseInt(page) - 1) * limit || 0;
    const posts = await Models.Post.findAll({
      where,
      limit: limit,
      offset: offset,
      order: [['sort', 'DESC']],
      include: [Models.BlogCategory, Models.Media],
    });
    return posts;
  }

  async search(title) {
    const where: any = {};
    if (title && title.length > 2) {
      where.title = { $like: `%${title}%` };
    }
    where.active = blogStatuses.active.code;
    const result = await Models.Post.findAll({
      where,
      order: [['sort', 'DESC']],
      include: [Models.BlogCategory, Models.Media],
    });
    return result;
  }

  async landing() {
    const posts = await Models.Post.findAll({
      where: {
        active: blogStatuses.active.code,
        isInLanding: true,
      },
      order: [['sort', 'DESC']],
      limit: 8,
      include: [Models.BlogCategory, Models.Media],
    });
    return posts;
  }

  async postListCount(id) {
    const count = await Models.Post.count({
      where: {
        blogCatId: id,
        active: blogStatuses.active.code,
      },
    });
    return count;
  }
}
