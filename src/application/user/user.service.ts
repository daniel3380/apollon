import { HttpException, HttpStatus, Injectable } from '@nestjs/common';

import axios from 'axios';
import moment from 'moment';
import { userObj } from '../../DTO/user.dto';
import { Jwt } from '../../common/helpers/jwt.helper';
import { Tools } from '../../common/helpers/tools.helpers';
import { SmsRqDataAccess } from '../../dataAccess/smsRq.dataAccess';
import { UserDataAccess } from '../../dataAccess/user.dataAccess';
import { validNumberWithRegex } from '../../utils/validPhoneNumber.regex';
import { faker } from '@faker-js/faker';
import smsRequest = require('request');

const env = process.env.NODE_ENV;

@Injectable()
export class UserService {
  constructor(
    private readonly smsRqDataAccess: SmsRqDataAccess,
    private readonly tools: Tools,
    private readonly jwt: Jwt,
    private readonly usersDataAccess: UserDataAccess,
  ) {}

  async login(loginDto, ip) {
    const phone = this.tools.removeZiro(loginDto.phone);
    const validNumber = validNumberWithRegex(phone);
    if (validNumber == false) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_ACCEPTABLE,
          error: 'شماره وارد شده معتبر نیست',
        },
        HttpStatus.NOT_ACCEPTABLE,
      );
    }

    const smsRq = await this.smsRqDataAccess.findSmsRq(phone, ip);
    if (parseInt(loginDto.type) === 1 || parseInt(loginDto.type) === 2) {
      if (smsRq) {
        if (smsRq.sendCount > 1000) {
          return smsRq.jwtToken;
        }

        // کد من
        const now = moment(new Date());
        const secondsDiff = now.diff(moment(smsRq.lastSentTime), 'seconds');
        if (secondsDiff < 120 && smsRq.mobile === phone) {
          return smsRq.jwtToken;
        }

        let jwtToken = smsRq.jwtToken;
        if (phone !== smsRq.mobile) {
          const tokenValues = {
            phone: phone,
          };
          jwtToken = this.jwt.signer(tokenValues, 2 * 24 * 60 * 60);
        }
        const sendCount = smsRq.sendCount + 1;

        const code = this.tools.codeCreator();

        if (env !== 'development' && env !== 'test') {
          if (parseInt(loginDto.type) === 1) {
            await this.tools.sendSmsCode(loginDto.phone, code);
          } else {
            await this.tools.callCode(loginDto.phone, code);
          }
        }

        await this.smsRqDataAccess.updateSmsRq(
          smsRq.id,
          jwtToken,
          phone,
          sendCount,
          ip,
          code,
        );

        return jwtToken;
      } else {
        const code = this.tools.codeCreator();

        const tokenValues = {
          phone: phone,
        };
        if (env !== 'development' && env !== 'test') {
          if (parseInt(loginDto.type) === 1) {
            await this.tools.sendSmsCode(loginDto.phone, code);
          } else {
            await this.tools.callCode(loginDto.phone, code);
          }
        }
        const jwtToken = this.jwt.signer(tokenValues);

        await this.smsRqDataAccess.createSmsRq(jwtToken, phone, ip, code);
        return jwtToken;
      }
    } else {
      throw new HttpException(
        {
          status: HttpStatus.NOT_ACCEPTABLE,
          error: ' نوع ارتباط به درستی وارد نشده است  ',
        },
        HttpStatus.NOT_ACCEPTABLE,
      );
    }
  }

  async verifyCode(tokenVerifyInput): Promise<any> {
    try {
      const { code, phone, token } = tokenVerifyInput;
      const phoneClean = this.tools.removeZiro(phone);
      const tokenValues = this.jwt.verifier(token);
      if (!tokenValues) {
        throw new HttpException(
          {
            status: HttpStatus.UNAUTHORIZED,
            error: ' توکن نا معتبر ',
          },
          HttpStatus.UNAUTHORIZED,
        );
      }

      const rMobile = tokenValues['phone'];
      if (phoneClean !== rMobile) {
        throw new HttpException(
          {
            status: HttpStatus.NOT_ACCEPTABLE,
            error: ' شماره موبایل تغییر کرده است ',
          },
          HttpStatus.NOT_ACCEPTABLE,
        );
      }
      const smsRq = await this.smsRqDataAccess.validateCode(
        phoneClean,
        parseInt(code),
        token,
      );
      if (!smsRq) {
        throw new HttpException(
          {
            status: HttpStatus.NOT_ACCEPTABLE,
            error: 'کد اشتباه می باشد',
          },
          HttpStatus.NOT_ACCEPTABLE,
        );
      }
      const userExist = await this.usersDataAccess.findOne(phoneClean);
      if (!userExist) {
        const user = await this.usersDataAccess.create(phoneClean);
        const tokenValues = {
          userId: user.id,
        };
        const jwtConf = this.jwt.signer(tokenValues, 24 * 60 * 60);
        await this.usersDataAccess.update(user.id, jwtConf);
        const userSubmit = await this.usersDataAccess.findOneId(user.id);
        await this.smsRqDataAccess.deleteSmsRq(smsRq.id);
        await axios
          .post(
            `https://abk-chat-test.iran.liara.run/api/v3/users/signup`,
            {
              firstName: userSubmit.firstName ?? 'null',
              lastName: userSubmit.lastName ?? 'null',
              username: userSubmit.username ?? 'null',
              phone: userSubmit.phone,
            },
            { headers: { 'Content-Type': 'application/json' } },
          )
          .then((res) => {
            console.log('res.data.message');
          })
          .catch((err) => {
            console.log('err');
          });
        return userSubmit;
      } else {
        const tokenValues = {
          userId: userExist.id,
        };
        const jwtConf = this.jwt.signer(tokenValues, 24 * 60 * 60 * 365);
        await this.usersDataAccess.update(userExist.id, jwtConf);
        const finalUser = await this.usersDataAccess.findOneId(userExist.id);
        await this.smsRqDataAccess.deleteSmsRq(smsRq.id);

        await axios
          .post(
            `https://abk-chat-test.iran.liara.run/api/v3/users/signup`,
            {
              firstName: finalUser.firstName ?? 'null',
              lastName: finalUser.lastName ?? 'null',
              username: finalUser.username ?? 'null',
              phone: finalUser.phone,
            },
            { headers: { 'Content-Type': 'application/json' } },
          )
          .then((res) => {
            console.log('res.data.message');
          })
          .catch((err) => {
            console.log('err');
          });
        // const body = ex.body;
        // const myJSON = JSON.parse(body);
        // console.log(myJSON);
        return finalUser;
      }
    } catch (err) {
      throw err;
    }
  }

  async tokenRenew(token) {
    const tokenValues = this.jwt.verifier(token);
    if (!tokenValues) {
      new HttpException(
        {
          status: HttpStatus.UNAUTHORIZED,
          error: ' توکن نامعتبر ',
        },
        HttpStatus.UNAUTHORIZED,
      );
      return;
    }
    // قبلی آیا اکسپایر می شود یا نه
    const user = await this.usersDataAccess.findUserToken(token);
    const jwtConf = this.jwt.signer({ phone: user.phone }, 24 * 60 * 60);
    user.update({ token: jwtConf });

    const finalUser = await this.usersDataAccess.findOneId(user.id);
    const url = `https://abk-chat.iran.liara.run/api/v3/users/submit-data`;
    const options = {
      url,
      method: 'POST',
      json: {
        user: finalUser,
      },
      data: finalUser,
    };
    await smsRequest(options);

    return;
  }

  async addNameFamily(NameFamilyDto, userId) {
    const { firstName, lastName } = NameFamilyDto;
    const userIsExist = await this.usersDataAccess.findOneId(userId);

    if (!userIsExist) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'کاربری یافت نشد',
        },
        HttpStatus.NOT_FOUND,
      );
    }

    const result = await this.usersDataAccess.addNameFamily(
      userIsExist.id,
      firstName,
      lastName,
    );

    const userFinal = await this.usersDataAccess.findOneId(userIsExist.id);

    await this.tools.sendDataChat(
      userFinal.phone,
      userFinal.id,
      userFinal.firstName + ' ' + userFinal.lastName,
    );

    const url = `https://abk-chat.iran.liara.run/api/v3/users/submit-data`;
    const options = {
      url,
      method: 'POST',
      json: {
        user: userFinal,
      },
      data: userFinal,
    };

    await smsRequest(options);

    return { success: true, result };
  }

  async addUsername(username: string, userId) {
    const uniqueUsername = await this.usersDataAccess.findUsername(username);
    if (uniqueUsername && uniqueUsername.id !== userId) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_ACCEPTABLE,
          error: ' این نام کاربری قبلا انتخاب شده است ',
        },
        HttpStatus.NOT_ACCEPTABLE,
      );
    }
    const result = await this.usersDataAccess.updateUsername(userId, username);
    const resultUser = await this.usersDataAccess.findOneId(userId);
    // await axios.post(
    //   'https://abk-chat.iran.liara.run/api/v3/users/submit-data',
    //   {
    //     user: resultUser,
    //   },
    // );
    // const ax = await axios.post(
    //   'https://abk-chat.iran.liara.run/api/v3/users/signup',
    //   {
    //     user: 'resultUser',
    //   },
    // );

    return;
  }

  async checkUsername(username: string, userId) {
    const uniqueUsername = await this.usersDataAccess.findUsername(username);
    if (uniqueUsername && uniqueUsername.id !== userId) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_ACCEPTABLE,
          error: ' این نام کاربری قبلا انتخاب شده است ',
        },
        HttpStatus.NOT_ACCEPTABLE,
      );
    }

    return { success: true };
  }

  async findOne(userId) {
    const user = await this.usersDataAccess.findOneId(userId);
    if (!user) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: ' کاربری وجود ندارد ',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    return userObj(user);
  }

  async userName(username) {
    const user = await this.usersDataAccess.findUsername(username);
    if (!user) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: ' کاربری وجود ندارد ',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    return userObj(user);
  }

  async sendAvatar(addAvatarDto, userId) {
    const { buffer } = addAvatarDto;
    const user = await this.usersDataAccess.findOneId(userId);
    if (!user) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: ' کاربری وجود ندارد ',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    // const srcImage = await this.cloud.getUploadsList(buffer, 'avatar');
    // const resAvatar = await this.usersDataAccess.avatar(srcImage, userId);

    // const url = `https://abk-chat.iran.liara.run/api/v3/users/submit-data`;
    // const options = {
    //   url,
    //   method: 'POST',
    //   json: {
    //     user: resAvatar,
    //   },
    //   data: resAvatar,
    // };
    // await smsRequest(options);

    // return resAvatar;
  }

  async updateStatus(userId, status) {
    const user = await this.usersDataAccess.findOne(userId);
    if (!user) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: ' کاربری وجود ندارد ',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    await this.usersDataAccess.update(userId, {
      userStatus: status,
      lastSeen: new Date(),
    });
    return user;
  }
}
