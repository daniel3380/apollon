import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Req,
  Res,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { Request, Response } from 'express';
import {
  AddUsernameDto,
  LoginBodyDto,
  LoginDtoResponse,
  LoginStepTwoDto,
  NameFamilyDto,
  UserDto,
  userObj,
} from '../../DTO/user.dto';

import { UserService } from './user.service';

import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiBody,
  ApiConsumes,
  ApiHeader,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { IpAddress } from '../../decorators/ipAddress.decorator';
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();

@ApiTags('user login')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}
  @ApiOperation({ summary: 'login' })
  @ApiHeader({
    name: 'jToken',
    description: 'this token received in login',
  })
  @ApiBody({
    type: LoginBodyDto,
    description: 'get body',
  })
  @Post('login')
  @HttpCode(HttpStatus.OK)
  async login(
    @Body() loginDto: LoginBodyDto,
    @IpAddress() ip: string,
    @Res() res: Response,
  ) {
    try {
      if (res.locals && res.locals.user) {
        const result = { res: res.locals.user, status: true };
        res.json(result);
        return;
      }
      const singInToken = await this.userService.login(loginDto, ip);
      res.status(200).json({ singInToken, status: false });
      return;
    } catch (err) {
      throw err;
    }
  }

  @ApiOperation({ summary: 'verify step two' })
  @ApiHeader({
    name: 'jToken',
    description: 'this token received in login',
  })
  @ApiBody({
    type: LoginStepTwoDto,
    description: 'get body',
  })
  @ApiOkResponse({
    description: 'response user',
    type: LoginDtoResponse,
  })
  @HttpCode(HttpStatus.OK)
  @Post('verify')
  async verifyCode(@Body() tokenDto: LoginStepTwoDto, @Res() res: Response) {
    try {
      if (res.locals && res.locals.user) {
        const result = { res: res.locals.user, status: true };
        res.json(result);
        return;
      }
      const user = await this.userService.verifyCode(tokenDto);
      res.status(200).json({ res: user, status: true });
      return;
    } catch (err) {
      throw err;
    }
  }

  @ApiOperation({ summary: 'renew token' })
  @ApiHeader({
    name: 'jToken',
    description: 'this token received in login',
  })
  @ApiOkResponse({
    description: 'response user',
    type: LoginDtoResponse,
  })
  @HttpCode(HttpStatus.OK)
  @Put('token-renew')
  async tokenRenew(@Res() res: Response, @Req() req: Request) {
    try {
      const user = await this.userService.tokenRenew(req.header('jToken'));
      res.status(200).json({ res: user, status: true });
      return;
    } catch (err) {
      throw err;
    }
  }

  @ApiOperation({ summary: 'send name family' })
  @ApiHeader({
    name: 'jToken',
    description: 'this token received in login',
  })
  @ApiBody({
    type: NameFamilyDto,
    description: 'get body',
  })
  @ApiOkResponse({
    description: 'response user',
    type: LoginDtoResponse,
  })
  @HttpCode(HttpStatus.OK)
  @Put('name-family')
  async nameFamily(@Body() nameFamilyDto: NameFamilyDto, @Res() res: Response) {
    try {
      await this.userService.addNameFamily(nameFamilyDto, res.locals.user.id);
      res.json({ res: true });
      return;
    } catch (err) {
      throw err;
    }
  }

  @ApiOperation({ summary: 'send userName' })
  @ApiHeader({
    name: 'jToken',
    description: 'this token received in login',
  })
  @ApiBody({
    type: AddUsernameDto,
    description: 'get body',
  })
  @ApiOkResponse({
    description: 'response user',
    type: LoginDtoResponse,
  })
  @HttpCode(HttpStatus.OK)
  @Post('add-username')
  async getUsername(
    @Body() addUsernameDto: AddUsernameDto,
    @Res() res: Response,
  ) {
    try {
      await this.userService.addUsername(
        addUsernameDto.username,
        res.locals.user.id,
      );
      res.json({ res: true });
      return;
    } catch (error) {
      throw error;
    }
  }

  @ApiOperation({ summary: 'check userName' })
  @ApiHeader({
    name: 'jToken',
    description: 'this token received in login',
  })
  @ApiBody({
    type: AddUsernameDto,
    description: 'get body',
  })
  @ApiOkResponse({
    description: 'response user',
    type: LoginDtoResponse,
  })
  @HttpCode(HttpStatus.OK)
  @Post('check-username')
  async checkUsername(
    @Body() addUsernameDto: AddUsernameDto,
    @Res() res: Response,
  ) {
    try {
      const result = await this.userService.checkUsername(
        addUsernameDto.username,
        res.locals.user.id,
      );
      res.json(result);
    } catch (error) {
      throw error;
    }
  }

  @ApiOperation({ summary: 'import avatar' })
  @ApiHeader({
    name: 'jToken',
    description: 'this token received in login',
  })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
        dist: { type: 'string' },
      },
    },
  })
  @ApiOkResponse({
    description: 'response user',
    type: UserDto,
  })
  @UseInterceptors(FileInterceptor('file'))
  @Post('sendAvatar')
  async sendAvatar(@UploadedFile() file, @Res() res: Response) {
    try {
      await this.userService.sendAvatar(file, res.locals.user.id);
      return res.status(200).json({ res: true });
    } catch (err) {
      throw err;
    }
  }

  @ApiOperation({ summary: 'check userName' })
  @ApiHeader({
    name: 'jToken',
    description: 'this token received in login',
  })
  @ApiOkResponse({
    description: 'response user',
    type: UserDto,
  })
  @Get('getUsername/:username')
  async userName(
    @Param('username')
    username: string,
  ) {
    try {
      return await this.userService.userName(username);
    } catch (err) {
      throw err;
    }
  }

  @ApiOperation({ summary: 'check userName' })
  @ApiHeader({
    name: 'jToken',
    description: 'this token received in login',
  })
  @ApiOkResponse({
    description: 'response user',
    type: UserDto,
  })
  @Get('getUser')
  async getUserToken(@Res() res: Response) {
    try {
      return res.status(200).json(userObj(res.locals.user));
    } catch (err) {
      throw err;
    }
  }

  @ApiOperation({ summary: 'check userName' })
  @ApiHeader({
    name: 'jToken',
    description: 'this token received in login',
  })
  @ApiOkResponse({
    description: 'response user',
    type: UserDto,
  })
  @Get('get/:userId')
  async findOne(
    @Param(
      'userId',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE }),
    )
    userId: number,
  ) {
    try {
      return await this.userService.findOne(userId);
    } catch (err) {
      throw err;
    }
  }
}
