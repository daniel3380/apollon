import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';

import { UserController } from './user.controller';
import { UserService } from './user.service';

import {
  CheckUserMiddleware,
  ValidUserMiddleware,
} from '../../common/middlewares/validateUser.middleware';

import { Jwt } from '../../common/helpers/jwt.helper';
import { Tools } from '../../common/helpers/tools.helpers';
import { SmsRqDataAccess } from '../../dataAccess/smsRq.dataAccess';
import { UserDataAccess } from '../../dataAccess/user.dataAccess';

@Module({
  imports: [],
  controllers: [UserController],
  providers: [UserService, Jwt, Tools, UserDataAccess, SmsRqDataAccess],
})
export class UsersModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(ValidUserMiddleware)
      .exclude('/user/login', '/user/verify')
      .forRoutes('/user');

    consumer
      .apply(CheckUserMiddleware)
      .forRoutes('/user/login', '/user/verify');
  }
}
