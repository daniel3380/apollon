import { IpAddress } from './../../../decorators/ipaddress.decorator';
import {
  Controller,
  HttpStatus,
  HttpCode,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Body,
  Req,
  Query,
  Res,
} from '@nestjs/common';
import { Request, Response } from 'express';

import {
  ApiTags,
  ApiOkResponse,
  ApiOperation,
  ApiBody,
  ApiHeader,
} from '@nestjs/swagger';
import {
  PostDto,
  getPostInputDto,
  ViewListPostDto,
  LandingResultDto,
  SearchParametr,
} from '../../../DTO/post.dto';
import { BlogService } from './blog.service';
import {
  CreatePostCommentDto,
  PostCommentDto,
} from '../../../DTO/postComment.dto';

@ApiTags('blog')
@Controller('blog')
export class BlogController {
  constructor(private readonly blogService: BlogService) {}
  // blogcat ***********************************************************
  @ApiOperation({ summary: 'postList' })
  @ApiOkResponse({
    description: 'list Posts',
    type: [ViewListPostDto],
  })
  @Get('postList')
  @HttpCode(HttpStatus.OK)
  async listPost(@Query() query: getPostInputDto): Promise<ViewListPostDto[]> {
    const list = await this.blogService.listPost(query);
    return list;
  }
  // post **********************************************************************
  @ApiOperation({ summary: 'Post' })
  @ApiOkResponse({
    description: 'Post info',
    type: [PostDto],
  })
  @Get('post/:slug')
  @HttpCode(HttpStatus.OK)
  async Post(
    @Param('slug')
    slug: string,
    @IpAddress() ip: string,
    @Res() res: Response,
  ): Promise<PostDto> {
    try {
      const post = await this.blogService.post(slug, res.locals.user, ip);
      res.json(post);
      return;
    } catch (err) {
      throw err;
    }
  }
  // create post comment **********************************************************
  @ApiOperation({ summary: 'Create PostComment' })
  @ApiOkResponse({
    description: 'Postcomment Dto',
    type: [PostCommentDto],
  })
  @ApiBody({
    type: CreatePostCommentDto,
    description: 'create postComment Dto',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @Post('postComment/:id')
  @HttpCode(HttpStatus.OK)
  async createPostComment(
    @Req() req: Request,
    @Body() createPostCommentDto: CreatePostCommentDto,
    @Param(
      'id',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE }),
    )
    id: number,
  ): Promise<PostCommentDto> {
    try {
      const userInf = req.session && req.session.user ? req.session.user : null;
      const postComment = await this.blogService.createPostComment(
        userInf,
        createPostCommentDto,
        id,
      );
      return postComment;
    } catch (err) {
      throw err;
    }
  }
  // reply post comment *********************************************************
  @ApiOperation({ summary: 'reply PostComment' })
  @ApiOkResponse({
    description: 'PostComment Dto',
    type: [PostCommentDto],
  })
  @ApiBody({
    type: CreatePostCommentDto,
    description: 'reply post comment',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @Post('postComment/reply/:id')
  @HttpCode(HttpStatus.OK)
  async replyPostComment(
    @Req() req: Request,
    @Body() createPostCommentDto: CreatePostCommentDto,
    @Param(
      'id',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE }),
    )
    id: number,
  ): Promise<PostCommentDto> {
    try {
      const userInf = req.session && req.session.user ? req.session.user : null;
      const postComment = await this.blogService.replyPostComment(
        userInf,
        createPostCommentDto,
        id,
      );
      return postComment;
    } catch (err) {
      throw err;
    }
  }
  // search blog ******************************************************************
  @ApiOperation({ summary: 'search blog' })
  @ApiOkResponse({
    description: 'search result',
    type: [LandingResultDto],
  })
  @Get('search')
  @HttpCode(HttpStatus.OK)
  async search(@Query() query: SearchParametr): Promise<LandingResultDto> {
    const result = await this.blogService.search(query);
    return result;
  }
  // landing content ************************************************************
  @ApiOperation({ summary: 'landing post & news' })
  @ApiOkResponse({
    description: 'landing content',
    type: [LandingResultDto],
  })
  @Get('landing')
  @HttpCode(HttpStatus.OK)
  async landingContent(): Promise<LandingResultDto> {
    const content = await this.blogService.landingContent();
    return content;
  }
  // postList count *****************************************************************
  @ApiOperation({ summary: 'postList count' })
  @HttpCode(HttpStatus.OK)
  @Get('postList/:catId')
  async postListCount(
    @Param(
      'catId',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE }),
    )
    catId: number,
  ): Promise<any> {
    try {
      const count = await this.blogService.postListCount(catId);
      return count;
    } catch (err) {
      throw err;
    }
  }
}
