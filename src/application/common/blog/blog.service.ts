import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { getAllBlogCategoriesObj } from '../../../DTO/blogCategory.dto';
import { CategoryDataAccess } from '../../../dataAccess/blogCategory.dataAccess';
import blogStatuses from '../../../common/eNums/blogStatuses.enum';
import { PostCommentDataAccess } from '../../../dataAccess/postComment.dataAccess';
import { PostDataAccess } from '../../../dataAccess/post.dataAccess';
import { PostVisitDataAccess } from '../../../dataAccess/postVisit.dataAccess';
import {
  postInfoObjDto,
  viewPostObjDto,
  landingResultObjDto,
} from '../../../DTO/post.dto';
import commentStatuses from '../../../common/eNums/commentStatuses.enum';
import { postCommentObj } from '../../../DTO/postComment.dto';

@Injectable()
export class BlogService {
  constructor(
    private readonly categoryDataAccess: CategoryDataAccess,
    private readonly postCommentDataAccess: PostCommentDataAccess,
    private readonly postDataAccess: PostDataAccess,
    private readonly postVisitDataAccess: PostVisitDataAccess,
  ) {}
  // blogCategory ******************************************************
  async listPost(query) {
    const listPost = await this.postDataAccess.listPosts(query);
    return listPost.map((item) => viewPostObjDto(item));
  }
  // post *****************************************************************
  async post(slug, user, ip) {
    const post = await this.postDataAccess.findPostBySlug(slug);
    if (!post) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'POST_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    } else {
      if (!user) {
        user = { id: null };
      }
      const visit = await this.postVisitDataAccess.findById(
        user.id,
        post.id,
        ip,
      );
      if (!visit) {
        if (user) {
          await this.postDataAccess.updateVisitCount(
            post.id,
            post.visitCount + 1,
          );
          await this.postVisitDataAccess.create(user.id, post.id, ip);
        } else {
          user = { id: null };
          await this.postDataAccess.updateVisitCount(
            post.id,
            post.visitCount + 1,
          );
          await this.postVisitDataAccess.create(user.id, post.id, ip);
        }
      }
      // await this.postDataAccess.updateVisitCount(post.id, post.visitCount + 1);
      if (post.PostComments.length !== 0) {
        post.PostComments = post.PostComments.filter(
          (x) => x.active === commentStatuses.published.code,
        );
      } else {
        post.PostComments = [];
      }
      return postInfoObjDto(post);
    }
  }
  // create postComment ***************************************************
  async createPostComment(userInf, createPostCommentDto, id) {
    const userName =
      userInf && userInf.username !== null ? userInf.username : 'user';
    const { text } = createPostCommentDto;
    const post = await this.postDataAccess.findById(id);
    if (!post || post.active !== blogStatuses.active.code) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'POST_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    } else {
      const postComment = await this.postCommentDataAccess.createPostComment(
        id,
        userName,
        text,
      );
      return postCommentObj(postComment);
    }
  }
  // blogCat structure ******************************************************
  async getAllBlogCategories() {
    const list = await this.categoryDataAccess.getAll();
    for (let i = 0; i < list.length; i++) {
      list[i].BlogCategories = list[i].BlogCategories.filter(
        (x) => x.active === blogStatuses.active.code,
      );
    }
    return list.map((item) => getAllBlogCategoriesObj(item));
  }
  // reoly post comment *******************************************************
  async replyPostComment(userInf, createPostCommentDto, id) {
    const postComment = await this.postCommentDataAccess.findById(id);
    if (!postComment || postComment.active !== commentStatuses.published.code) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'COMMENT_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    } else {
      const { text } = createPostCommentDto;
      const userName =
        userInf && userInf.username !== null ? userInf.username : 'user';
      const replyComment = await this.postCommentDataAccess.replyComment(
        postComment.postId,
        id,
        userName,
        text,
      );
      return postCommentObj(replyComment);
    }
  }
  // search blog *************************************************************
  async search(query) {
    const { title } = query;
    const resultPosts = await this.postDataAccess.search(title);
    return landingResultObjDto(resultPosts);
  }
  // landing content *********************************************************
  async landingContent() {
    const landingPost = await this.postDataAccess.landing();
    return landingResultObjDto(landingPost);
  }
  // post list count *********************************************************
  async postListCount(catId) {
    const count = await this.postDataAccess.postListCount(catId);
    return count;
  }
}
