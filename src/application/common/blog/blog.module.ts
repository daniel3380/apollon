import { Module } from '@nestjs/common';

import { BlogController } from './blog.controller';
import { BlogService } from './blog.service';
import { PostDataAccess } from '../../../dataAccess/post.dataAccess';
import { CategoryDataAccess } from '../../../dataAccess/blogCategory.dataAccess';
import { PostCommentDataAccess } from '../../../dataAccess/postComment.dataAccess';
import { PostVisitDataAccess } from '../../../dataAccess/postVisit.dataAccess';

@Module({
  imports: [],
  controllers: [BlogController],
  providers: [
    BlogService,
    CategoryDataAccess,
    PostDataAccess,
    PostCommentDataAccess,
    PostVisitDataAccess,
  ],
})
export class BlogModule {}
