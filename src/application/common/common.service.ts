import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import * as shell from 'shelljs';

import { Tools } from '../../common/helpers/tools.helpers';
import { MediaDataAccess } from '../../dataAccess/media.dataAccess';
import { StateDataAccess } from '../../dataAccess/state.dataAccess';
import { UserDataAccess } from '../../dataAccess/user.dataAccess';
// import { Cloud } from '../../upload/getUploadsList.service';

import { StateDto, stateObj } from '../../DTO/state.dto';

import * as fs from 'fs';
import paths from '../../config/paths';

@Injectable()
export class CommonService {
  constructor(
    private readonly mediaDataAccess: MediaDataAccess,
    private readonly stateDataAccess: StateDataAccess,
    private readonly tools: Tools,
    // private readonly cloud: Cloud,
    private readonly userDataAccess: UserDataAccess,
  ) {}
  // uploadfile ***************************************************
  async upload(addAvatarDto, userId) {
    const { buffer } = addAvatarDto;

    const user = await this.userDataAccess.findOneId(userId);
    if (!user) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: ' کاربری وجود ندارد ',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    // const srcImage = await this.cloud.getUploadsList(buffer, 'avatar');
    // const file = await this.mediaDataAccess.createMediaCloud(srcImage, userId);

    // // const resAvatar = await this.userDataAccess.avatar(srcImage, userId);
    // return file;
  }
  async uploadWithCompress(filename, dist) {
    !fs.existsSync(`${paths.staticFiles}${paths.uploadedFiles}${dist}`)
      ? fs.mkdir(`${paths.staticFiles}${paths.uploadedFiles}${dist}`, (err) => {
          console.log(err);
        })
      : null;
    const mimeType = await this.tools.checkMime(
      `${paths.staticFiles}${paths.uploadedFiles}${filename}`,
    );
    if (mimeType === 1) {
      await shell.exec(
        'sudo  ffmpeg -i   ' +
          `${paths.staticFiles}${paths.uploadedFiles}${filename}` +
          ' -vf scale=w=720:h=380:force_original_aspect_ratio=decrease ' +
          `${paths.staticFiles}${paths.uploadedFiles}${dist}/${filename}` +
          '  2>&1',
      );
      fs.unlinkSync(`${paths.staticFiles}${paths.uploadedFiles}${filename}`);
    } else if (mimeType === 2) {
      await shell.exec(
        'sudo  ffmpeg -i   ' +
          `${paths.staticFiles}${paths.uploadedFiles}${filename}` +
          '  -vf scale=480:-1  ' +
          `${paths.staticFiles}${paths.uploadedFiles}${dist}/${filename}` +
          '  2>&1',
      );
      fs.unlinkSync(`${paths.staticFiles}${paths.uploadedFiles}${filename}`);
    } else {
      fs.copyFile(
        `${paths.staticFiles}${paths.uploadedFiles}${filename}`,
        `${paths.staticFiles}${paths.uploadedFiles}${dist}/${filename}`,
        (err) => {
          if (err) console.log(err);
          fs.unlinkSync(
            `${paths.staticFiles}${paths.uploadedFiles}${filename}`,
          );
        },
      );
    }

    const mediaUrl = `${paths.uploadedFiles}${dist}/${filename}`;
    const file = await this.mediaDataAccess.createMedia(mediaUrl, mimeType);
    return file;
  }
  async deleteMedia(mediaId) {
    const media = await this.mediaDataAccess.findById(mediaId);
    if (media && media.ownerId === 0) {
      const fileUrl = `${paths.staticFiles}/${media.mediaUrl}`;
      try {
        fs.unlinkSync(fileUrl);
        await this.mediaDataAccess.deleteMedia(mediaId);
        return true;
      } catch (err) {
        console.error(err);
        return false;
      }
    }
    return false;
  }
  async findAllStates(): Promise<StateDto[]> {
    const states = await this.stateDataAccess.findAll();
    return states.map((state) => stateObj(state));
  }
}
