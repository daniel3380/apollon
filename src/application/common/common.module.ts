import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';

import { Jwt } from '../../common/helpers/jwt.helper';
import { Tools } from '../../common/helpers/tools.helpers';
import { ValidUserMiddleware } from '../../common/middlewares/validateUser.middleware';
import { MediaDataAccess } from '../../dataAccess/media.dataAccess';
import { StateDataAccess } from '../../dataAccess/state.dataAccess';
import { UserDataAccess } from '../../dataAccess/user.dataAccess';
import { CommonController } from './common.controller';
import { CommonService } from './common.service';
import { BlogModule } from './blog/blog.module';

@Module({
  imports: [BlogModule],
  controllers: [CommonController],
  providers: [
    CommonService,
    Tools,
    MediaDataAccess,
    StateDataAccess,
    UserDataAccess,
    Jwt,
  ],
})
export class CommonModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(ValidUserMiddleware)
      .exclude('/user/login', '/user/verify')
      .forRoutes('/user');

    consumer.apply(ValidUserMiddleware).forRoutes('/blog/postComment');
  }
}
