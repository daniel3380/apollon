import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Res,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBody,
  ApiConsumes,
  ApiHeader,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { Response } from 'express';

import { StateDto } from '../../DTO/state.dto';
import { CommonService } from './common.service';

import { FileInterceptor } from '@nestjs/platform-express';

@ApiTags('commons')
@Controller('commons')
export class CommonController {
  constructor(private readonly commonService: CommonService) {}
  // upload file *********************************************************************************
  // @ApiOkResponse({
  //   description: 'user default info',
  //   type: [MediaDto],
  // })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
        dist: { type: 'string' },
      },
    },
  })
  @Post('/uploadFile')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'jToken',
    description: 'this token received in signIn',
  })
  @UseInterceptors(FileInterceptor('file'))
  async uploadMultipleFiles(
    @UploadedFile() file,
    @Res() res: Response,
    @Body() body,
  ): Promise<any> {
    if (file) {
      // const media = await this.commonService.upload(file.filename, body.dist);
      const media = await this.commonService.upload(file, res.locals.user.id);
      return res.status(201).json(media);
    }
    throw new HttpException(
      {
        status: HttpStatus.BAD_REQUEST,
        error: 'BAD_REQUEST',
      },
      HttpStatus.BAD_REQUEST,
    );
  }
  // delete uploaded media   *******************
  @ApiOperation({ summary: 'delete uploaded media' })
  @ApiNotFoundResponse({ description: 'file not found' })
  @ApiInternalServerErrorResponse({
    description: 'delete file failed',
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'status',
    type: Boolean,
  })
  @ApiHeader({
    name: 'jToken',
    description: 'this token received in signIn',
  })
  @Delete('/:mediaId')
  @HttpCode(HttpStatus.OK)
  async secoundDepartments(
    @Param(
      'mediaId',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE }),
    )
    mediaId: number,
  ): Promise<boolean> {
    try {
      const status = await this.commonService.deleteMedia(mediaId);
      return status;
    } catch (err) {
      throw err;
    }
  }
  // All states   *******************
  @ApiOperation({ summary: 'find all states with cities' })
  @ApiNotFoundResponse({ description: 'no state found' })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'states',
    type: [StateDto],
  })
  @Get('/states')
  @HttpCode(HttpStatus.OK)
  async states(): Promise<StateDto[]> {
    try {
      const states = await this.commonService.findAllStates();
      return states;
    } catch (err) {
      throw err;
    }
  }
}
