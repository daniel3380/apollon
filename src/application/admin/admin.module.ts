import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';

import { AdminController } from './admin.controller';
import { AdminsService } from './admin.service';

import { Jwt } from '../../common/helpers/jwt.helper';
import { Tools } from '../../common/helpers/tools.helpers';
import { BlogModule } from './blog/blog.module';
import { AdminsDataAccess } from '../../dataAccess/admins.dataAccess';
import { SmsRqDataAccess } from '../../dataAccess/smsRq.dataAccess';
import {
  CheckAdminMiddleware,
  ValidAdminMiddleware,
} from '../../common/middlewares/validateAdmin.middleware';

@Module({
  imports: [BlogModule],
  controllers: [AdminController],
  providers: [AdminsService, AdminsDataAccess, SmsRqDataAccess, Jwt, Tools],
})
export class AdminModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ValidAdminMiddleware).forRoutes('admins/blog');

    consumer.apply(CheckAdminMiddleware).forRoutes(
      {
        path: 'admins',
        method: RequestMethod.POST,
      },
      {
        path: 'admins/logOut',
        method: RequestMethod.GET,
      },
      {
        path: 'admins/forgetPass',
        method: RequestMethod.PUT,
      },
      {
        path: 'admins/recoverPass',
        method: RequestMethod.PATCH,
      },
    );
  }
}
