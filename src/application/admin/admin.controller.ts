import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Req,
  Res,
  Get,
  Put,
  UsePipes,
  Patch,
  Headers,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiInternalServerErrorResponse,
  ApiBody,
  ApiHeader,
  ApiOperation,
} from '@nestjs/swagger';
import { Request, Response } from 'express';
import { AdminsService } from './admin.service';
import {
  AdminDto,
  AdminForgetPasswordDto,
  AdminLoginDto,
  adminObj,
  AdminRecoverPasswordDto,
  AdminUpdatePasswordDto,
} from '../../DTO/admin.dto';
import { MobilePipe } from '../../pipes/mobile.pipe';
import { IpAddress } from '../../decorators/ipAddress.decorator';
import { CodePipe } from '../../pipes/code.pipe';

@ApiTags('admins')
@Controller('admins')
export class AdminController {
  constructor(private readonly adminsService: AdminsService) {}
  // login ************************************
  @ApiOkResponse({
    description: 'admin login successful',
    type: [AdminDto],
  })
  @ApiNotFoundResponse({ description: 'admin not found' })
  @ApiInternalServerErrorResponse({
    description: 'login faild',
  })
  @Post('')
  @HttpCode(HttpStatus.OK)
  async login(
    @Req() req: Request,
    @Body() login: AdminLoginDto,
  ): Promise<AdminDto> {
    try {
      if (req.session && req.session.admin) {
        return req.session.admin;
      }
      const admin = await this.adminsService.login(
        login.username,
        login.password,
      );
      req.session.admin = admin;
      return admin;
    } catch (err) {
      throw err;
    }
  }
  // logOut Admin
  @ApiOkResponse({
    description: 'logOut',
    type: Boolean,
  })
  @ApiNotFoundResponse({ description: 'admin not found' })
  @ApiInternalServerErrorResponse({
    description: 'logout faild',
  })
  @HttpCode(HttpStatus.OK)
  @Get('logOut')
  async logOut(@Req() req: Request, @Res() res: Response): Promise<boolean> {
    try {
      const { admin } = res.locals;
      if (admin) {
        await this.adminsService.logOut(admin.id);
      }
      req.session.destroy((e) => console.log(e));
      res.json(true);
      return;
    } catch (err) {
      throw err;
    }
  }
  // dashboard ************************************
  @ApiOperation({
    summary: ' گرفتن اطلاعات ادمین ',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token received in login',
  })
  @ApiOkResponse({
    description: 'get admin info successful',
    type: AdminDto,
  })
  @Get('')
  @HttpCode(HttpStatus.OK)
  async dash(@Res() res: Response): Promise<AdminDto> {
    try {
      if (res.locals.admin) {
        res.json(adminObj(res.locals.admin));
        return;
      }
    } catch (err) {
      throw err;
    }
  }
  // forgetPassword ************************************
  @ApiOkResponse({
    description: 'admin forget password',
    type: [String],
  })
  @ApiBody({
    type: AdminForgetPasswordDto,
    description: 'update admin password',
  })
  @ApiNotFoundResponse({ description: 'admin not found' })
  @ApiInternalServerErrorResponse({
    description: 'recovery faild',
  })
  @UsePipes(new MobilePipe())
  @Put('forgetPass')
  @HttpCode(HttpStatus.OK)
  async forgetPassword(
    @IpAddress() ip: string,
    @Req() req: Request,
    @Res() res: Response,
    @Body() body,
  ): Promise<any> {
    try {
      if (req.session && req.session.admin) {
        res.json(req.session.admin);
        return;
      }
      const { mobile } = body;
      const recoverPassToken = await this.adminsService.forgetPassword(
        mobile,
        ip,
      );
      res.json({ recoverPassToken });
      return;
    } catch (err) {
      throw err;
    }
  }
  // recover Password ************************************
  @ApiOkResponse({
    description: 'admin recover password',
    type: AdminDto,
  })
  @ApiNotFoundResponse({ description: 'admin not found' })
  @ApiInternalServerErrorResponse({
    description: 'recovery faild',
  })
  @ApiHeader({
    name: 'recover_pass_token',
    description: 'this token recieved in forgetPassword',
  })
  @UsePipes(new CodePipe())
  @Patch('recoverPass')
  @HttpCode(HttpStatus.OK)
  async recoverPassword(
    @Headers() headers,
    @Req() req: Request,
    @Body() recoverPass: AdminRecoverPasswordDto,
  ): Promise<AdminDto> {
    try {
      if (req.session && req.session.admin) {
        return req.session.admin;
      }

      const { recover_pass_token } = headers;
      const admin = await this.adminsService.recoverPassword(
        recoverPass,
        recover_pass_token,
      );
      return admin;
    } catch (err) {
      throw err;
    }
  }
  // updatePassword ************************************
  @ApiOkResponse({
    description: 'update password',
    type: Boolean,
  })
  @ApiBody({
    type: AdminUpdatePasswordDto,
    description: 'update admin password',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @Put('updatePassword')
  @HttpCode(HttpStatus.OK)
  async updatePassword(
    @Req() req: Request,
    @Res() res: Response,
    @Body() updatePasswordDto: AdminUpdatePasswordDto,
  ) {
    try {
      const { admin } = res.locals;
      await this.adminsService.updatePassword(
        updatePasswordDto.oldPassword,
        updatePasswordDto.newPassword,
        admin.username,
      );
      res.json(true);
      return;
    } catch (err) {
      throw err;
    }
  }
}
