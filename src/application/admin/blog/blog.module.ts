import { Module } from '@nestjs/common';

import { BlogController } from './blog.controller';
import { BlogService } from './blog.service';
import { PostDataAccess } from '../../../dataAccess/post.dataAccess';
import { CategoryDataAccess } from '../../../dataAccess/blogCategory.dataAccess';
import { AdminsDataAccess } from '../../../dataAccess/admins.dataAccess';
import { MediaDataAccess } from '../../../dataAccess/media.dataAccess';
import { PostMediaDataAccess } from '../../../dataAccess/postMedia.dataAccess';
import { RelatedPostDataAccess } from '../../../dataAccess/relatedPost.dataAccess';
import { PostCommentDataAccess } from '../../../dataAccess/postComment.dataAccess';
import { ConvertDate } from '../../../common/helpers/convertDate.helper';
import { Tools } from '../../../common/helpers/tools.helpers';
import { Jwt } from '../../../common/helpers/jwt.helper';

@Module({
  controllers: [BlogController],
  providers: [
    BlogService,
    AdminsDataAccess,
    MediaDataAccess,
    CategoryDataAccess,
    RelatedPostDataAccess,
    PostCommentDataAccess,
    PostDataAccess,
    PostMediaDataAccess,
    ConvertDate,
    Tools,
    Jwt,
  ],
})
export class BlogModule {}
