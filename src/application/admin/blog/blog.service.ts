/* eslint-disable object-curly-newline */
/* eslint-disable prefer-const */
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import bluebirdPromise = require('bluebird');
import pathes from '../../../config/paths';
import { CategoryDataAccess } from '../../../dataAccess/blogCategory.dataAccess';
import { PostDataAccess } from '../../../dataAccess/post.dataAccess';
import { MediaDataAccess } from '../../../dataAccess/media.dataAccess';
import { PostMediaDataAccess } from '../../../dataAccess/postMedia.dataAccess';
import { RelatedPostDataAccess } from '../../../dataAccess/relatedPost.dataAccess';
import { PostCommentDataAccess } from '../../../dataAccess/postComment.dataAccess';
import { Tools } from '../../../common/helpers/tools.helpers';
import { blogCatArrObjDto, blogCatObjDto } from '../../../DTO/blogCategory.dto';
import { postObjDto, postInfoObjDto } from '../../../DTO/post.dto';
import blogStatuses from '../../../common/eNums/blogStatuses.enum';
import commentStatuses from '../../../common/eNums/commentStatuses.enum';
import { relatedPostObjDto } from '../../../DTO/relatedPost.dto';
import {
  postCommentArrObjDto,
  postCommentObj,
  postCommentInfoObj,
} from '../../../DTO/postComment.dto';

@Injectable()
export class BlogService {
  constructor(
    private readonly mediaDataAccess: MediaDataAccess,
    private readonly categoryDataAccess: CategoryDataAccess,
    private readonly postDataAccess: PostDataAccess,
    private readonly relatedPostDataAccess: RelatedPostDataAccess,
    private readonly postMediaDataAccess: PostMediaDataAccess,
    private readonly postCommentDataAccess: PostCommentDataAccess,
    private readonly tools: Tools,
  ) {}
  // create post ******************************************************************
  async createPost(createPostDto, admin) {
    const {
      blogCatId,
      mediaId,
      title,
      description,
      text,
      writer,
      mediaAlt,
      metaTitle,
      metaDescription,
      metaKeywords,
      isInLanding,
      postMedias,
    } = createPostDto;
    const blogCat = await this.categoryDataAccess.findById(blogCatId);
    if (!blogCat) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'BLOG_CATEGORY_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    if (blogCat.BlogCategories.length !== 0) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_ACCEPTABLE,
          error: 'BLOG_CATEGORY_NOT_ACCEPTABLE',
        },
        HttpStatus.NOT_ACCEPTABLE,
      );
    }
    const slug = this.tools.convertToSlug(title);
    const checkPost = await this.postDataAccess.checkDuplicate(slug);
    if (checkPost) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          error: 'TITLE_CONFLICT',
        },
        HttpStatus.CONFLICT,
      );
    }
    const media = await this.mediaDataAccess.findById(mediaId);
    if (!media) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'MEDIA_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const post = await this.postDataAccess.createPostRequest(
      admin.id,
      blogCatId,
      mediaId,
      title,
      description,
      slug,
      text,
      writer,
      mediaAlt,
      metaTitle,
      metaDescription,
      metaKeywords,
      isInLanding,
    );
    await this.postDataAccess.updateSortPost(post.id);
    if (
      typeof postMedias === 'object' &&
      postMedias.constructor === Array &&
      postMedias.length > 0
    ) {
      await bluebirdPromise.map(postMedias, async (media) => {
        const item = await this.mediaDataAccess.findById(media.mediaId);
        if (item) {
          const postMedia = await this.postMediaDataAccess.create(
            media.mediaId,
            post.id,
            media.title,
          );
          this.mediaDataAccess.updateMediaOwner(
            postMedia.id,
            'PostMedia',
            media.mediaId,
          );
        }
      });
    }
    this.mediaDataAccess.updateMediaOwner(post.id, 'Post', mediaId);
    return postObjDto(await this.postDataAccess.findById(post.id));
  }
  // update post *********************************************************************
  async updatePost(updatePostDto, admin) {
    const {
      id,
      mediaId,
      title,
      description,
      text,
      writer,
      mediaAlt,
      metaTitle,
      metaDescription,
      metaKeywords,
      isInLanding,
      postMedias,
    } = updatePostDto;
    let { blogCatId } = updatePostDto;
    const post = await this.postDataAccess.findById(id);
    if (!post) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'POST_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    if (blogCatId) {
      const blogCat = await this.categoryDataAccess.findById(blogCatId);
      if (!blogCat) {
        throw new HttpException(
          {
            status: HttpStatus.NOT_FOUND,
            error: 'BLOG_CATEGORY_NOT_FOUND',
          },
          HttpStatus.NOT_FOUND,
        );
      }
      if (blogCat.BlogCategories.length !== 0) {
        throw new HttpException(
          {
            status: HttpStatus.NOT_ACCEPTABLE,
            error: 'BLOG_CATEGORY_NOT_ACCEPTABLE',
          },
          HttpStatus.NOT_ACCEPTABLE,
        );
      }
    } else {
      blogCatId = post.blogCatId;
    }
    const media = await this.mediaDataAccess.findById(mediaId);
    if (!media) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'MEDIA_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const checkTitle = await this.postDataAccess.checkTitle(title);
    if (checkTitle && title !== post.title) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          error: 'TITLE_CONFLICT',
        },
        HttpStatus.CONFLICT,
      );
    }
    await this.postDataAccess.updatePost(
      id,
      admin.id,
      blogCatId,
      mediaId,
      title,
      description,
      text,
      writer,
      mediaAlt,
      metaTitle,
      metaDescription,
      metaKeywords,
      isInLanding,
    );
    if (
      typeof postMedias === 'object' &&
      postMedias.constructor === Array &&
      postMedias.length > 0
    ) {
      await bluebirdPromise.map(postMedias, async (media) => {
        const item = await this.mediaDataAccess.findById(media.mediaId);
        const postMedia = await this.postMediaDataAccess.find(
          post.id,
          media.mediaId,
        );
        if (postMedia) {
          this.postMediaDataAccess.update(postMedia.id, media.title);
        }
        if (item && !postMedia) {
          const postMedia = await this.postMediaDataAccess.create(
            media.mediaId,
            post.id,
            media.title,
          );
          this.mediaDataAccess.updateMediaOwner(
            postMedia.id,
            'PostMedia',
            media.mediaId,
          );
        }
      });
    }
  }
  // post detail ***********************************************************************
  async postDetail(postId) {
    const postDetail = await this.postDataAccess.findById(postId);
    if (postDetail) {
      postDetail.PostComments = [];
      return postInfoObjDto(postDetail);
    } else {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'POST_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
  }
  // detele postMedia ***************************************************************************
  async deletePostMedia(deletePostMediaDto) {
    const { id, postId } = deletePostMediaDto;
    const media = await this.postMediaDataAccess.findPostMedia(id, postId);
    if (!media) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'POSTMEDIA_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    await this.mediaDataAccess.updateMediaOwner(0, '', media.mediaId);
    await this.postMediaDataAccess.deletePostMedia(media.id);
  }
  // change status post ********************************************************************
  async changeStatusPost(postStatusDto) {
    const { id, active } = postStatusDto;
    const post = await this.postDataAccess.findById(id);
    if (!post) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'POST_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const checkStatus = Object.keys(blogStatuses).some(
      (key) => blogStatuses[key].code === active,
    );
    if (!checkStatus) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_ACCEPTABLE,
          error: 'INVALIED_STATUS',
        },
        HttpStatus.NOT_ACCEPTABLE,
      );
    }
    await this.postDataAccess.changeStatusPost(id, active);
  }
  // list posts ***************************************************************************
  async listPosts(filterListPostDto) {
    const list = await this.postDataAccess.filterlistAndFilter(
      filterListPostDto,
    );
    return list.map((item) => postObjDto(item));
  }
  //change Sort post *************************************************************************
  async changeSortPost(filterListPostDto, changesortpostDto) {
    const { fromId, toId } = changesortpostDto;
    await this.postDataAccess.changeSortPost(fromId, toId);
    const list = await this.postDataAccess.filterlistAndFilter(
      filterListPostDto,
    );
    return list.map((item) => postObjDto(item));
  }
  // create category *************************************************************************
  async createCategory(createBlogCatDto) {
    let { parentId } = createBlogCatDto;
    const { title, description } = createBlogCatDto;
    if (parentId && parentId !== 0) {
      const parent = await this.categoryDataAccess.findById(parentId);
      if (!parent) {
        throw new HttpException(
          {
            status: HttpStatus.NOT_FOUND,
            error: 'PARENTID_NOT_FOUND',
          },
          HttpStatus.NOT_FOUND,
        );
      }
      if (parent.Posts.length !== 0) {
        throw new HttpException(
          {
            status: HttpStatus.NOT_ACCEPTABLE,
            error: 'PARENTID_NOT_ACCEPTABLE',
          },
          HttpStatus.NOT_ACCEPTABLE,
        );
      }
    } else {
      parentId = 0;
    }
    const checkTitle = await this.categoryDataAccess.checkDuplicate(title);
    if (checkTitle) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          error: 'TITLE_CONFLICT',
        },
        HttpStatus.CONFLICT,
      );
    }
    const category = await this.categoryDataAccess.createCategory(
      parentId,
      title,
      description,
    );
    return blogCatObjDto(category);
  }
  // list category ****************************************************************************
  async blogCatList(getParentForListDto) {
    const { parentId } = getParentForListDto;

    const categories = await this.categoryDataAccess.findAll(parentId);

    const categoryList = await bluebirdPromise.map(
      categories,
      async (category) => {
        return blogCatArrObjDto(category);
      },
    );
    return categoryList;
  }
  // update category *************************************************************************
  async updateBlogCategory(updateBlogCategory) {
    let { parentId } = updateBlogCategory;
    const { id, title, description } = updateBlogCategory;
    const category = await this.categoryDataAccess.findById(id);
    if (!category) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'CATEGORY_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    if (parentId && parentId !== 0 && parentId !== id) {
      const parent = await this.categoryDataAccess.findById(parentId);
      if (!parent) {
        throw new HttpException(
          {
            status: HttpStatus.NOT_FOUND,
            error: 'PARENTID_NOT_FOUND',
          },
          HttpStatus.NOT_FOUND,
        );
      }
      if (parent.Posts.length !== 0) {
        throw new HttpException(
          {
            status: HttpStatus.NOT_ACCEPTABLE,
            error: 'PARENTID_NOT_ACCEPTABLE',
          },
          HttpStatus.NOT_ACCEPTABLE,
        );
      }
    } else {
      parentId = 0;
    }
    const checkTitle = await this.categoryDataAccess.checkDuplicate(title);
    if (checkTitle && title !== category.title) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          error: 'TITLE_CONFLICT',
        },
        HttpStatus.CONFLICT,
      );
    }
    await this.categoryDataAccess.updateBlogCategory(
      id,
      parentId,
      title,
      description,
    );
  }
  // change status category *************************************************************************
  async changeStatusCategory(satusBlogCatDto) {
    const { id, active } = satusBlogCatDto;
    const category = await this.categoryDataAccess.findById(id);
    if (!category) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'CATEGORY_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const checkStatus = Object.keys(blogStatuses).some(
      (key) => blogStatuses[key].code === active,
    );
    if (!checkStatus) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_ACCEPTABLE,
          error: 'INVALIED_STATUS',
        },
        HttpStatus.NOT_ACCEPTABLE,
      );
    }
    await this.categoryDataAccess.changeStatusCategory(id, active);
  }
  // create RelatedPost ***************************************************************************
  async createRelatedPost(createRelatedPostDto) {
    let { postId, mediaId, title, url, description } = createRelatedPostDto;
    if (postId && postId !== 0) {
      const post = await this.postDataAccess.findById(postId);
      if (!post) {
        throw new HttpException(
          {
            status: HttpStatus.NOT_FOUND,
            error: 'POST_NOT_FOUND',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        mediaId = post.mediaId;
        title = post.title;
        description = post.description;
        url = `${pathes.siteUrl}/blog/${post.slug}`;
      }
    } else if (mediaId && mediaId !== 0) {
      const media = await this.mediaDataAccess.findById(mediaId);
      if (!media) {
        throw new HttpException(
          {
            status: HttpStatus.NOT_FOUND,
            error: 'MEDIA_NOT_FOUND',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        postId = null;
      }
    } else {
      throw new HttpException(
        {
          status: HttpStatus.NOT_ACCEPTABLE,
          error: 'REQUEST_NOT_ACCEPTABLE',
        },
        HttpStatus.NOT_ACCEPTABLE,
      );
    }
    const relatedPost = await this.relatedPostDataAccess.createRequest(
      postId,
      mediaId,
      title,
      url,
      description,
    );
    this.mediaDataAccess.updateMediaOwner(
      relatedPost.id,
      'RelatedPost',
      mediaId,
    );
    return relatedPostObjDto(
      await this.relatedPostDataAccess.findById(relatedPost.id),
    );
  }
  // update RelatedPost ****************************************************************************
  async updateRelatedPost(updatedRelatedPost) {
    let { id, postId, mediaId, title, url, description } = updatedRelatedPost;
    const item = await this.relatedPostDataAccess.findById(id);
    if (!item) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'RELATED_POST_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    if (postId && postId !== 0) {
      const post = await this.postDataAccess.findById(postId);
      if (!post) {
        throw new HttpException(
          {
            status: HttpStatus.NOT_FOUND,
            error: 'POST_NOT_FOUND',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        mediaId = post.mediaId;
        title = post.title;
        description = post.description;
        url = `${pathes.siteUrl}/blog/${post.slug}`;
      }
    } else {
      postId = null;
      if (mediaId && mediaId !== 0 && item.mediaId !== mediaId) {
        const media = await this.mediaDataAccess.findById(mediaId);
        if (!media) {
          throw new HttpException(
            {
              status: HttpStatus.NOT_FOUND,
              error: 'MEDIA_NOT_FOUND',
            },
            HttpStatus.NOT_FOUND,
          );
        }
        this.mediaDataAccess.updateMediaOwner(0, '', item.mediaId);
      } else {
        mediaId = item.mediaId;
      }
      title = !title ? item.title : title;
      description = !description ? item.description : title;
      url = !url ? item.url : url;
    }
    await this.relatedPostDataAccess.updateRequest(
      id,
      postId,
      mediaId,
      title,
      url,
      description,
    );
    this.mediaDataAccess.updateMediaOwner(id, 'RelatedPost', mediaId);
  }
  // delete RelatedPost ****************************************************************************
  async deleteRelatedPost(id) {
    const item = await this.relatedPostDataAccess.findById(id);
    if (!item) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'RELATED_POST_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    } else {
      await this.relatedPostDataAccess.deleteRequest(id);
      return true;
    }
  }
  //list post comment *****************************************************************************
  async listPostComments(filterListPostCommentDto) {
    const list = await this.postCommentDataAccess.listAndFilter(
      filterListPostCommentDto,
    );
    return list.map((item) => postCommentArrObjDto(item));
  }
  // change status post comment ********************************************************************
  async changeStatusPostComment(statusPostCommentDto) {
    const { id, active } = statusPostCommentDto;
    const postComment = await this.postCommentDataAccess.findById(id);
    if (!postComment) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'POSTCOMMENT_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const checkStatus = Object.keys(commentStatuses).some(
      (key) => commentStatuses[key].code === active,
    );
    if (!checkStatus) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_ACCEPTABLE,
          error: 'INVALIED_STATUS',
        },
        HttpStatus.NOT_ACCEPTABLE,
      );
    }
    await this.postCommentDataAccess.changeStatus(id, active);
  }
  // get detail post comment by id *****************************************************************
  async postCommentDetail(id) {
    const postComment = await this.postCommentDataAccess.findById(id);
    if (!postComment) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'POSTCOMMENT-NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    } else {
      return postCommentInfoObj(postComment);
    }
  }
  // reply post Comment ****************************************************************************
  async replyPostComment(createPostCommentDto, id, admin) {
    const postComment = await this.postCommentDataAccess.findById(id);
    if (!postComment || postComment.active !== commentStatuses.published.code) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'COMMENT_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    } else {
      const { text } = createPostCommentDto;
      const userName = admin.username ? admin.username : 'admin';
      const replyPostComment = await this.postCommentDataAccess.replyComment(
        postComment.postId,
        id,
        userName,
        text,
      );
      return postCommentObj(replyPostComment);
    }
  }
}
