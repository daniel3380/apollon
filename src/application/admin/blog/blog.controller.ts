import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Res,
  Get,
  Put,
  Query,
  Patch,
  Param,
  ParseIntPipe,
  Delete,
} from '@nestjs/common';
import { Response } from 'express';
import {
  ApiTags,
  ApiOkResponse,
  ApiHeader,
  ApiBody,
  ApiOperation,
} from '@nestjs/swagger';
import {
  BlogCatDto,
  CreateBlogCatDto,
  UpdateBlogCatDto,
  StatusBlogCatDto,
  BlogCatListDto,
  getParentListBlogCatDto,
} from '../../..//DTO/blogCategory.dto';
import {
  PostDto,
  CreatePostDto,
  UpdatePostDto,
  PostStatusDto,
  ListPostDto,
  FilterListPostDto,
  ChangeSortPostDto,
} from '../../../DTO/post.dto';
import { DeletePostMediaDto } from '../../../DTO/postMedia.dto';
import {
  RelatedPostDto,
  CreateRelatedPostDto,
  UpdatedRelatedPost,
} from '../../../DTO/relatedPost.dto';
import {
  StatusPostCommentDto,
  ListPostCommentDto,
  PostCommentDto,
  CreatePostCommentDto,
  FilterListPostCommentDto,
  PostCommentInfoDto,
} from '../../../DTO/postComment.dto';
import { BlogService } from './blog.service';

@ApiTags('admin blog')
@Controller('admins/blog')
export class BlogController {
  constructor(private readonly blogService: BlogService) {}
  // create new post **************************************************************************
  @ApiOperation({ summary: 'create new post' })
  @ApiOkResponse({
    description: 'PostDto info',
    type: [PostDto],
  })
  @ApiBody({
    type: CreatePostDto,
    description: 'create new post',
  })
  @Post('post')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'jtoken',
    description: 'this token received in signIn',
  })
  async createPost(
    @Res() res: Response,
    @Body() createPostDto: CreatePostDto,
  ): Promise<PostDto> {
    try {
      const post = await this.blogService.createPost(
        createPostDto,
        res.locals.admin,
      );
      res.status(HttpStatus.OK).json(post);
      return;
    } catch (err) {
      throw err;
    }
  }
  // update post ******************************************************************************
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'update post' })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token received in signIn',
  })
  @ApiOkResponse({
    description: 'update post',
    type: Boolean,
  })
  @ApiBody({
    type: UpdatePostDto,
    description: 'update post DTO',
  })
  @Put('post')
  @HttpCode(HttpStatus.OK)
  async updatePost(
    @Res() res: Response,
    @Body() updatePostDto: UpdatePostDto,
  ): Promise<any> {
    try {
      await this.blogService.updatePost(updatePostDto, res.locals.admin);
      res.json({ status: true });
      return;
    } catch (err) {
      throw err;
    }
  }
  // change status of post ********************************************************************
  @ApiOperation({ summary: 'change status of post' })
  @ApiOkResponse({
    description: 'api status',
    type: Boolean,
  })
  @ApiBody({
    type: PostStatusDto,
    description: 'change Status of post',
  })
  @Patch('post')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'jtoken',
    description: 'this token received in signIn',
  })
  async changeStatusPost(@Body() postStatusDto: PostStatusDto): Promise<any> {
    try {
      await this.blogService.changeStatusPost(postStatusDto);
      return { status: true };
    } catch (err) {
      throw err;
    }
  }
  // Filter / list post ***********************************************************************
  @ApiOperation({ summary: ' Filter / list post ' })
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'jtoken',
    description: 'this token received in signIn',
  })
  @ApiOkResponse({
    description: 'posts list',
    type: [ListPostDto],
  })
  @Get('post')
  @HttpCode(HttpStatus.OK)
  async ListPosts(
    @Query() filterListPostDto: FilterListPostDto,
  ): Promise<ListPostDto[]> {
    const blogService = await this.blogService.listPosts(filterListPostDto);
    return blogService;
  }
  // get  detail post by id *****************************************************************************
  @ApiOperation({ summary: 'detail post' })
  @ApiOkResponse({
    description: 'post detail info',
    type: PostDto,
  })
  @Get('post/:postId')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'jtoken',
    description: 'this token received in signIn',
  })
  async postDetail(
    @Param(
      'postId',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE }),
    )
    postId: number,
  ): Promise<PostDto> {
    try {
      const postDetail = await this.blogService.postDetail(postId);
      return postDetail;
    } catch (err) {
      throw err;
    }
  }
  // delete post media ************************************************************************
  @ApiOperation({ summary: 'delete post media' })
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'jtoken',
    description: 'this token received in signIn',
  })
  @ApiOkResponse({
    description: 'detele post media',
    type: Boolean,
  })
  @ApiBody({
    type: DeletePostMediaDto,
    description: 'delete post media info',
  })
  @Delete('post/deletePostMedia')
  @HttpCode(HttpStatus.OK)
  async detelePostMedia(
    @Body() deletePostMediaDto: DeletePostMediaDto,
  ): Promise<any> {
    try {
      await this.blogService.deletePostMedia(deletePostMediaDto);
      return { status: true };
    } catch (err) {
      throw err;
    }
  }
  // change sort post ************************************************************************
  @ApiOperation({ summary: 'change sort post' })
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'jtoken',
    description: 'this token received in signIn',
  })
  @ApiOkResponse({
    description: 'change sort post',
    type: [ListPostDto],
  })
  @ApiBody({
    type: ChangeSortPostDto,
    description: 'change sort post',
  })
  @Put('post/sort')
  @HttpCode(HttpStatus.OK)
  async changeSortPost(
    @Body() changesortpostDto: ChangeSortPostDto,
    @Query() filterListPostDto: FilterListPostDto,
  ): Promise<ListPostDto[]> {
    try {
      const listPosts = await this.blogService.changeSortPost(
        filterListPostDto,
        changesortpostDto,
      );
      return listPosts;
    } catch (err) {
      throw err;
    }
  }
  // create new blogCat ***********************************************************************
  @ApiOperation({ summary: ' create blog category' })
  @ApiOkResponse({
    description: 'blogCatagoryDto info',
    type: [BlogCatDto],
  })
  @ApiBody({
    type: CreateBlogCatDto,
    description: 'create new blog category',
  })
  @Post('category')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'jtoken',
    description: 'this token received in signIn',
  })
  async createCategory(
    @Body() createBlogCatDto: CreateBlogCatDto,
  ): Promise<BlogCatDto> {
    try {
      const category = await this.blogService.createCategory(createBlogCatDto);
      return category;
    } catch (err) {
      throw err;
    }
  }
  // list blogCat ***********************************************************************
  @ApiOperation({ summary: ' list blog category' })
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'jtoken',
    description: 'this token received in signIn',
  })
  @ApiOkResponse({
    description: 'blog category list',
    type: [BlogCatListDto],
  })
  @Get('category')
  @HttpCode(HttpStatus.OK)
  async blogCatList(
    @Query() getParentForListDto: getParentListBlogCatDto,
  ): Promise<BlogCatListDto[]> {
    try {
      const categories = await this.blogService.blogCatList(
        getParentForListDto,
      );
      return categories;
    } catch (err) {
      throw err;
    }
  }
  // update blogCat **********************************************************************
  @ApiOperation({ summary: ' update blog category' })
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'jtoken',
    description: 'this token received in signIn',
  })
  @ApiOkResponse({
    description: 'update blog category',
    type: Boolean,
  })
  @ApiBody({
    type: UpdateBlogCatDto,
    description: 'update blog category',
  })
  @Put('category')
  @HttpCode(HttpStatus.OK)
  async updateBlogCategory(
    @Body() updateBlogCategory: UpdateBlogCatDto,
  ): Promise<any> {
    try {
      await this.blogService.updateBlogCategory(updateBlogCategory);
      return { status: true };
    } catch (err) {
      throw err;
    }
  }
  //change status of blog Category **********************************************************************
  @ApiOperation({ summary: ' change status blog category' })
  @ApiOkResponse({
    description: 'api status',
    type: Boolean,
  })
  @ApiBody({
    type: StatusBlogCatDto,
    description: 'change Status of blog category',
  })
  @Patch('category')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'jtoken',
    description: 'this token received in signIn',
  })
  async changeStatusCategory(
    @Body() satusBlogCatDto: StatusBlogCatDto,
  ): Promise<any> {
    try {
      await this.blogService.changeStatusCategory(satusBlogCatDto);
      return { status: true };
    } catch (err) {
      throw err;
    }
  }
  // create relatedPost ******************************************************************************
  @ApiOperation({ summary: 'create RelatedPost' })
  @ApiOkResponse({
    description: 'RelatedPost Dto',
    type: [RelatedPostDto],
  })
  @ApiBody({
    type: CreateRelatedPostDto,
    description: 'Create RelatedPost Dto',
  })
  @Post('post/relatedPost')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'jtoken',
    description: 'this token received in signIn',
  })
  async createRelatedPost(
    @Body() createRelatedPostDto: CreateRelatedPostDto,
  ): Promise<RelatedPostDto> {
    try {
      const RelatedPost = await this.blogService.createRelatedPost(
        createRelatedPostDto,
      );
      return RelatedPost;
    } catch (err) {
      throw err;
    }
  }
  // update relatedPost **********************************************************************
  @ApiOperation({ summary: 'update RelatedPost' })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token received in signIn',
  })
  @ApiOkResponse({
    description: 'update RelatedPost',
    type: Boolean,
  })
  @ApiBody({
    type: UpdatedRelatedPost,
    description: 'update Related Post DTO',
  })
  @Put('post/relatedPost')
  @HttpCode(HttpStatus.OK)
  async updateRelatedPost(
    @Res() res: Response,
    @Body() updatedRelatedPost: UpdatedRelatedPost,
  ): Promise<any> {
    try {
      await this.blogService.updateRelatedPost(updatedRelatedPost);
      res.json({ status: true });
      return;
    } catch (err) {
      throw err;
    }
  }
  // delete RelatedPost ************************************************************************
  @ApiOperation({ summary: 'delete RelatedPost' })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token received in signIn',
  })
  @ApiOkResponse({
    description: 'deleted RelatedPost',
    type: Boolean,
  })
  @Delete('post/relatedPost/:id')
  @HttpCode(HttpStatus.OK)
  async deleteRelatedPost(
    @Param(
      'id',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE }),
    )
    id: number,
  ): Promise<boolean> {
    try {
      const status = await this.blogService.deleteRelatedPost(id);
      return status;
    } catch (err) {
      throw err;
    }
  }

  // Filter/ list post comment ********************************************************************
  @ApiOperation({ summary: 'Filter/ list PostComments' })
  @ApiOkResponse({
    description: 'postComments List',
    type: [ListPostCommentDto],
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token received in signIn',
  })
  @HttpCode(HttpStatus.OK)
  @Get('postComment')
  async listPostComment(
    @Query() filterListPostCommentDto: FilterListPostCommentDto,
  ): Promise<ListPostCommentDto[]> {
    const list = await this.blogService.listPostComments(
      filterListPostCommentDto,
    );
    return list;
  }
  //get post comment detail by id ****************************************************************
  @ApiOperation({ summary: 'datail PostComment' })
  @ApiOkResponse({
    description: 'post comment deatil info',
    type: PostCommentInfoDto,
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token received in signIn',
  })
  @HttpCode(HttpStatus.OK)
  @Get('postComment/:id')
  async postCommentDetail(
    @Param(
      'id',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE }),
    )
    id: number,
  ): Promise<PostCommentInfoDto> {
    try {
      const postCommentDetail = await this.blogService.postCommentDetail(id);
      return postCommentDetail;
    } catch (err) {
      throw err;
    }
  }
  // changeStatus postComment *********************************************************************
  @ApiOperation({ summary: 'change status PostComment' })
  @ApiOkResponse({
    description: 'api status',
    type: Boolean,
  })
  @ApiBody({
    type: StatusPostCommentDto,
    description: 'change status of postComment',
  })
  @Patch('postComment')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'jtoken',
    description: 'this token received in signIn',
  })
  async changeStatusPostComment(
    @Body() statusPostCommentDto: StatusPostCommentDto,
  ): Promise<any> {
    try {
      await this.blogService.changeStatusPostComment(statusPostCommentDto);
      return { status: true };
    } catch (err) {
      throw err;
    }
  }
  // reply post comment *****************************************************
  @ApiOperation({ summary: 'reply PostComment' })
  @ApiOkResponse({
    description: 'PostComment Dto',
    type: [PostCommentDto],
  })
  @ApiBody({
    type: CreatePostCommentDto,
    description: 'reply post comment',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token received in login',
  })
  @Post('postComment/reply/:id')
  @HttpCode(HttpStatus.OK)
  async replyPostComment(
    @Res() res: Response,
    @Body() createPostCommentDto: CreatePostCommentDto,
    @Param(
      'id',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE }),
    )
    id: number,
  ): Promise<PostCommentDto> {
    try {
      const postComment = await this.blogService.replyPostComment(
        createPostCommentDto,
        id,
        res.locals.admin,
      );
      res.json({ postComment });
      return;
    } catch (err) {
      throw err;
    }
  }
}
