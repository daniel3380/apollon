import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import md5 = require('md5');
import * as moment from 'moment';
import { AdminDto, adminObj } from '../../DTO/admin.dto';
import { Jwt } from '../../common/helpers/jwt.helper';
import { Tools } from '../../common/helpers/tools.helpers';
import { AdminsDataAccess } from '../../dataAccess/admins.dataAccess';
import { SmsRqDataAccess } from '../../dataAccess/smsRq.dataAccess';

@Injectable()
export class AdminsService {
  constructor(
    private readonly adminsDataAccess: AdminsDataAccess,
    private readonly smsRqDataAccess: SmsRqDataAccess,
    private readonly jwt: Jwt,
    private readonly tools: Tools,
  ) {}
  // login ***************************************************
  async login(username: string, password: string): Promise<AdminDto> {
    const admin = await this.adminsDataAccess.findByUserName(username);
    if (!admin) {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'LOGIN_FAILED',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    if (admin.password !== md5(password)) {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'LOGIN_FAILED',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    const tokenValues = {
      adminId: admin.id,
      type: 'admin',
    };
    const token = this.jwt.signer(tokenValues, 86400 * 2);
    await this.adminsDataAccess.updateJwtToken(token, admin.id);
    return adminObj(await this.adminsDataAccess.findByUserName(username));
  }
  // updatePassword **************************************
  async updatePassword(oldPassword, newPassword, username) {
    const admin = await this.adminsDataAccess.findByUserName(username);
    if (admin.password === md5(oldPassword)) {
      await this.adminsDataAccess.updatePassword(admin.id, md5(newPassword));
      return;
    }
    throw new HttpException(
      {
        status: HttpStatus.UNAUTHORIZED,
        error: 'UPDATE PASSWORD FORBIDEN',
      },
      HttpStatus.UNAUTHORIZED,
    );
  }
  // forget password *************************************
  async forgetPassword(mobile, ip) {
    const admin = await this.adminsDataAccess.findByMobile(mobile);
    if (admin) {
      const smsRq = await this.smsRqDataAccess.findSmsRq(mobile, ip);
      if (smsRq) {
        if (smsRq.sendCount > 4) {
          return smsRq.jwtToken;
        }
        const now = moment(new Date());
        const secondsDiff = now.diff(moment(smsRq.lastSentTime), 'seconds');
        if (secondsDiff < 120) {
          return smsRq.jwtToken;
        }
        const code = this.tools.codeCreator();
        let token = smsRq.jwtToken;
        if (mobile !== smsRq.mobile) {
          this.tools.sendSms(mobile, code);
          const tokenValues = {
            mobile,
          };
          token = this.jwt.signer(tokenValues, 86400 * 2);
        }
        const sendCount = smsRq.sendCount + 1;
        await this.tools.sendSmsCode(mobile, code);
        await this.smsRqDataAccess.updateSmsRq(
          smsRq.id,
          token,
          mobile,
          sendCount,
          ip,
          code,
        );
        return token;
      } else {
        const code = this.tools.codeCreator();
        this.tools.sendSms(mobile, code);
        const tokenValues = {
          mobile,
        };
        const token = this.jwt.signer(tokenValues, 86400 * 7);
        await this.smsRqDataAccess.createSmsRq(token, mobile, ip, code);
        return token;
      }
    }
    throw new HttpException(
      {
        status: HttpStatus.FORBIDDEN,
        error: 'RECOVER_PASSWORD_FORBIDEN',
      },
      HttpStatus.FORBIDDEN,
    );
  }
  // recover password *************************************
  async recoverPassword(recoverPass, recoverPassToken) {
    const { code, newPassword } = recoverPass;
    if (!recoverPassToken) {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'FORBIDDEN_HEARED',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    const tokenValues = this.jwt.verifier(recoverPassToken);
    if (!tokenValues) {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'FORBIDDEN_token_not_valid',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    const mobile = tokenValues['mobile'];
    const smsRq = await this.smsRqDataAccess.validateCode(
      mobile,
      code,
      recoverPassToken,
    );
    if (smsRq) {
      const admin = await this.adminsDataAccess.findByMobile(mobile);
      await this.adminsDataAccess.updatePassword(admin.id, md5(newPassword));
      const tokenValues = {
        adminId: admin.id,
        type: 'admin',
      };
      const token = this.jwt.signer(tokenValues, 86400 * 7);
      await this.adminsDataAccess.updateJwtToken(token, admin.id);
      return adminObj(
        await this.adminsDataAccess.findByUserName(admin.username),
      );
    }
    throw new HttpException(
      {
        status: HttpStatus.FORBIDDEN,
        error: 'FORBIDDEN',
      },
      HttpStatus.FORBIDDEN,
    );
  }
  async logOut(adminId) {
    await this.adminsDataAccess.logOut(adminId);
    return;
  }
}
